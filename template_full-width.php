<?php
/*
Template Name: Full Width
*/
get_header();


if ( is_tree('19') ) {
    // the page is "About", or the parent of the page is "About"
	include('headers/WW-header.php');

// } elseif ( is_tree('17') ) {
// 	include('headers/CR-header.php');
} elseif ( is_tree('2703') ) {
	include('headers/M-header.php');

} elseif ( is_tree('21') ) {
	include('headers/SG-header.php');

} elseif ( is_tree('20') ) {
	include('headers/FM-header.php');

} else {
    include('headers/alt-header.php'); // just in case we are at an unclassified page, perhaps the home page
}

?>






  <div id="content">
  	<div id="centercolumn">

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<H1><?php the_title(); ?></H1>
				<div class="entry">
					<?php the_content('Read the rest of this entry &raquo;'); ?>
				</div>


			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>


	<?php endif; ?>
</div>

<?php get_footer(); ?>
