<?php
get_header();
include('headers/SG-header.php');
$term = get_queried_object();
?>

  <div id="content">
  	<div id="leftcolumn">
	<h1 class="staff"><?php echo $term->name; ?></h1>
		<?php if (have_posts()) :
			while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2 class="posts"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<hr />
				<small><?php
					the_time('F jS, Y');
					echo ' | ';
					if ( function_exists('wp_print') )
						print_link();
					echo get_the_term_list( get_the_ID(), 'series', ' | Series: ', ', ', '' );
					?>
				</small>

				<div class="entry">
					<a class="bodylink" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
					<?php the_post_thumbnail( 'staff' ); ?></a>
					<?php the_content('Read the rest of this entry &raquo;'); ?>


				</div><?php if( function_exists('ADDTOANY_SHARE_SAVE_KIT') ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>

				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &rarr;', '1 Comment &rarr;', '% Comments &rarr;'); ?></p>

			</div>
				<hr class="bottomhr" />
		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>

<?php endif;
wp_reset_query();
?>



  	</div>
<div id="rightcolumn">

<?php
	include('sidebars/SG-sidebar.php');
?>

</div>

<?php
    include('footers/SG-footer.php');
?>
