
<h2 class="staff"><?php the_title(); ?>'s<br />Favorites:</h2><br />

	<?php
if(has_book_tag(null)) { ?>
<h3>Books</h3>

<?php
$taxonomy = 'books';
$terms = get_the_terms( $post->ID , $taxonomy, '','','' );

if ( !empty( $terms ) ) :


foreach ( $terms as $term ) {
	$link = get_term_link( $term, $taxonomy );
	if ( !is_wp_error( $link ) )
		echo '<a href="http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=' .$term->name . '">' . $term->name . '</a>';
}
endif;
}



if(has_verse_tag(null)) { ?>
<h3>Verses</h3>

<?php
$taxonomy = 'verses';
$terms = get_the_terms( $post->ID , $taxonomy, '','','' );

if ( !empty( $terms ) ) :


foreach ( $terms as $term ) {
	$link = get_term_link( $term, $taxonomy );
	if ( !is_wp_error( $link ) )
		echo '<a href="http://biblegateway.com/passage/?search=' .$term->name . '&version=NIV">' . $term->name . '</a>';
}
endif;
}



if(has_movie_tag(null)) { ?>
<h3>Movies</h3>

<?php
$taxonomy = 'movies';
$terms = get_the_terms( $post->ID , $taxonomy, '','','' );

if ( !empty( $terms ) ) :


foreach ( $terms as $term ) {
	$link = get_term_link( $term, $taxonomy );
	if ( !is_wp_error( $link ) )
		echo '<a href="http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=' .$term->name . '">' . $term->name . '</a>';
}
endif;
}



if(has_artist_tag(null)) { ?>
<h3>Music Artists</h3>

<?php
$taxonomy = 'artists';
$terms = get_the_terms( $post->ID , $taxonomy, '','','' );

if ( !empty( $terms ) ) :


foreach ( $terms as $term ) {
	$link = get_term_link( $term, $taxonomy );
	if ( !is_wp_error( $link ) )
		echo '<a href="http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=' .$term->name . '">' . $term->name . '</a>';
}
endif;
}




echo get_the_term_list($post->ID,  'foods', '<h3>Foods</h3>', '', '');

echo get_the_term_list($post->ID,  'activities', '<h3>Activities</h3>', '', ''); 

echo get_the_term_list($post->ID,  'favorite-things', '<h3>Other Favorites</h3>', '', '');

	if ($twittername == '' && $facebookurl == '' && $blogurl == '')
	{} else { ?>
	<h3>Social Media</h3>
	
	<?php $twittername = get_post_meta($post->ID, '_twitter', true);
	if ($twittername == '')
	{} else { ?>
	<a href="http://twitter.com/<?php echo get_post_meta($post->ID, "_twitter", true); ?>">Twitter</a>
	<?php } ;
	$facebookurl = get_post_meta($post->ID, '_facebook', true);
	if ($facebookurl == '')
	{} else { ?>
	<a href="<?php echo get_post_meta($post->ID, "_facebook", true); ?>">Facebook</a>
	<?php };
	$blogurl = get_post_meta($post->ID, '_blog', true);
	if ($blogurl == '')
	{} else { ?>
	<a href="<?php echo get_post_meta($post->ID, "_blog", true); ?>">Blog</a>
	<?php }; 
	}




?>

