<?php
if ( function_exists('register_sidebar') ) {
register_sidebar(array('name'=>'Weekend Worship',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h2>',
'after_title' => '</h2>',
'id' => 'w',
));
register_sidebar(array('name'=>'Family Ministry',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h2>',
'after_title' => '</h2>',
'id' => 'fm',
));
register_sidebar(array('name'=>'Small Groups',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h2>',
'after_title' => '</h2>',
'id' => 'sg',
));
register_sidebar(array('name'=>'Safe Harbor',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h2>',
'after_title' => '</h2>',
'id' => 'sh',
));
register_sidebar(array('name'=>'Sidebar Alternate',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h2>',
'after_title' => '</h2>',
'id' => 'sa',
));
register_sidebar(array('name'=>'Sermon Media',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h2>',
'after_title' => '</h2>',
'id' => 'sm',
));
register_sidebar(array('name'=>'GC News',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h2>',
'after_title' => '</h2>',
'id' => 'gc',
));
}

//Custom Login Page
function custom_login() {
echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/custom-login.css" />';
}
add_action('login_head', 'custom_login');

// Constant Contact Widget

class ConstantContact extends WP_Widget {
    /** constructor */
    function ConstantContact() {
        parent::WP_Widget(false, $name = 'Constant Contact');
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?>
<div id="wp_email_capture" class="textwidget">
 If you would like to receive the GC News Newsletter

<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post">
<input type="hidden" name="llr" value="584xm7bab">
<input type="hidden" name="m" value="1101538262910">
<input type="hidden" name="p" value="oi">


<label class="wp-email-capture-email">Email:</label>
<input name="ea" type="text" class="wp-email-capture-email"><br/>

<label class="buttons">
<button name="go" type="submit" value="Go">Submit</button></label>

</form>
</div>
              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
        $title = esc_attr($instance['title']);
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
        <?php
    }

} // class ConstantContact

// register ConstantContact widget
add_action('widgets_init', create_function('', 'return register_widget("ConstantContact");'));


// Church Home Survey Widget

class ChurchHomeSurvey extends WP_Widget {
    /** constructor */
    function ChurchHomeSurvey() {
        parent::WP_Widget(false, $name = 'Church Home Survey');
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?>
<div class="textwidget survey">
Do you consider GC your church home?

<form class="jotform-form" action="http://www.jotform.com/submit.php" method="post" name="form_3065144529" id="3065144529" accept-charset="utf-8">
<input type="hidden" name="formID" value="3065144529" />

<label><input type="radio" class="form-radio" id="input_1_0" name="q1_doYou" value="Yes" /> Yes </label>
<label><input type="radio" class="form-radio" id="input_1_1" name="q1_doYou" value="No" /> No </label>
<label><input type="radio" class="form-radio" id="input_1_2" name="q1_doYou" value="I need more information" /> I need more information </label>

<label class="buttons">
<button name="go" type="submit" value="Go">Submit</button></label>

<input type="hidden" id="simple_spc" name="simple_spc" value="3065144529" />
</form>
</div>
              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
        $title = esc_attr($instance['title']);
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
        <?php
    }

} // class ChurchHomeSurvey

// register ChurchHomeSurvey widget
add_action('widgets_init', create_function('', 'return register_widget("ChurchHomeSurvey");'));



// FacebookWired Wired
class FacebookWired extends WP_Widget {
    /** constructor */
    function FacebookWired() {
        parent::WP_Widget(false, $name = 'Facebook: Wired');
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        ?>
              <?php echo $before_widget; ?>
			  <a href="http://www.facebook.com/pages/Southport-NC/gcWIRED/132495010100725?ref=ts" target="_blank">
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?></a>

<script src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php" type="text/javascript"></script><script type="text/javascript">FB.init("969ebb8d40f6b28008b8236b0593b275");</script><fb:fan profile_id="132495010100725" stream="0" connections="8" width="165" height="352" css="http://generationschurch.com/fb.css"></fb:fan>

              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
        $title = esc_attr($instance['title']);
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
        <?php
    }

} // class FacebookWired

// register FacebookWired widget
add_action('widgets_init', create_function('', 'return register_widget("FacebookWired");'));



//FacebookGC
class FacebookGC extends WP_Widget {
    /** constructor */
    function FacebookGC() {
        parent::WP_Widget(false, $name = 'Facebook: GC');
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        ?>
              <?php echo $before_widget; ?>
			  <a href="https://www.facebook.com/GenerationsChurchNC" target="_blank">
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?></a>

<script src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php" type="text/javascript"></script><script type="text/javascript">FB.init("969ebb8d40f6b28008b8236b0593b275");</script><fb:fan profile_id="267033057547" stream="0" connections="8" width="165" height="352" css="http://generationschurch.com/fb.css"></fb:fan>

              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
        $title = esc_attr($instance['title']);
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
        <?php
    }

} // class FacebookGC

// register FacebookGC widget
add_action('widgets_init', create_function('', 'return register_widget("FacebookGC");'));



// change footer
function remove_footer_admin () {
echo 'Fueled by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | Site created by <a href="http://dsgnwrks.pro" target="_blank">DesignWorks</a>';
}
add_filter('admin_footer_text', 'remove_footer_admin');

// gravatar default
add_filter( 'avatar_defaults', 'newgravatar' );

function newgravatar ($avatar_defaults) {
$myavatar = get_bloginfo('template_directory') . '/images/steps.gif';
$avatar_defaults[$myavatar] = "GC";
return $avatar_defaults;
}

//hook the administrative header logo
add_action('admin_head, 'my_custom_logo');
function my_custom_logo() {
echo '
<style type="text/css">
#header-logo { background-image: url('.get_bloginfo('template_directory').'/images/admin_logo_steps.gif) !important; }
#wpadminbar .quicklinks li#wp-admin-bar-my-account-with-avatar > a img {width:16px; height 16px;}
</style>
';
}

// favicon
function blog_favicon() {
			  switch_to_blog(1);
			  echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('wpurl').'/favicon.ico" />';
			  restore_current_blog();
}
add_action('wp_head', 'blog_favicon');

// remove user registration fields
add_filter('user_contactmethods','hide_profile_fields',10,1);

function hide_profile_fields( $contactmethods ) {
unset($contactmethods['aim']);
unset($contactmethods['jabber']);
unset($contactmethods['yim']);
return $contactmethods;
}

// Add User registration fields
function my_new_contactmethods( $contactmethods ) {
// Add Twitter
$contactmethods['twitter'] = 'Twitter';
//add Facebook
$contactmethods['facebook'] = 'Facebook';

return $contactmethods;
}
add_filter('user_contactmethods','my_new_contactmethods',10,1);


// Add Staff Page link
add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

function my_show_extra_profile_fields( $user ) { ?>

	<h3>Important profile information</h3>

	<table class="form-table">

		<tr>
			<th><label for="staff">Staff Page Link</label></th>

			<td>
				<input type="text" name="staff" id="staff" value="<?php echo esc_attr( get_the_author_meta( 'staff', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please write your first and last name, no caps with a dash (-) inbetween.  <br />e.g. "first-last", "bob-smith"</span>
			</td>
		</tr>

	</table>
<?php }

add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	update_usermeta( $user_id, 'staff', $_POST['staff'] );
}


// guest-author custom field
add_filter( 'the_author', 'guest_author_name' );
add_filter( 'get_the_author_display_name', 'guest_author_name' );

function guest_author_name( $name ) {
global $post;

$author = get_post_meta( $post->ID, 'guest-author', true );

if ( $author )
$name = $author;

return $name;
}

// more secure login error reporting
add_filter('login_errors',create_function('$a', "return null;"));

// featured-image sizes
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 50, 50, true );
add_image_size( 'featured-items', 530, 78, true );
add_image_size( 'current-series', 796, 448, true );
add_image_size( 'series-art', 700, 394, true );
add_image_size( 'staff', 200, 1000 );
add_image_size( 'gc-news', 700, 1000 );

remove_filter('the_excerpt', 'wpautop');

// Addd Navigation Menus
add_theme_support( 'menus' );

	register_nav_menus( array(
		'W' => __( 'Weekend Worship Navigation', 'GCTheme' ),
	) );

	register_nav_menus( array(
		'SH' => __( 'Safe Harbor Navigation', 'GCTheme' ),
	) );

	register_nav_menus( array(
		'SG' => __( 'Small Groups Navigation', 'GCTheme' ),
	) );

	register_nav_menus( array(
		'FM' => __( 'Family Ministry Navigation', 'GCTheme' ),
	) );

	register_nav_menus( array(
		'default' => __( 'Default Navigation', 'GCTheme' ),
	) );

		register_nav_menus( array(
		'top' => __( 'Top Navigation', 'GCTheme' ),
	) );

function is_tree($pid) {      // $pid = The ID of the page we're looking for pages underneath
	global $post;         // load details about this page
	$anc = get_post_ancestors( $post->ID );
	foreach($anc as $ancestor) {
		if(is_page() && $ancestor == $pid) {
			return true;
		}
	}
	if(is_page()&&(is_page($pid)))
               return true;   // we're at the page or at a sub page
	else
               return false;  // we're elsewhere
};



// Custom Post Type Registration

add_action('init', 'cpt_register');

function cpt_register() {
        global $blog_id;


    $wwargs = array(
		'label' => __('Large Featured'),
        'singular_label' => __('Large Featured'),
		'public' => true,
		'exclude_from_search' => true,
        'show_ui' => true,
        'capability_type' => 'post',
	    '_builtin' => false,
		'menu_position' => 20,
        'hierarchical' => false,
        'query_var' => true,
'supports' => array('title', 'editor', 'thumbnail')
       );

    $featuredargs = array(
		'label' => __('Small Featured'),
        'singular_label' => __('Small Featured'),
		'public' => true,
		'exclude_from_search' => true,
        'show_ui' => true,
        'capability_type' => 'post',
	    '_builtin' => false,
		'menu_position' => 20,
        'hierarchical' => false,
        'query_var' => true,
'supports' => array('title', 'excerpt', 'thumbnail')
       );

	$labels = array(
		'name' => _x('Sermon Media', 'post type general name'),
		'singular_name' => _x('Sermon Media', 'post type singular name'),
		'add_new' => _x('Add New', 'Sermon Media Post'),
		'add_new_item' => __('Add New Sermon Media Post'),
		'edit_item' => __('Edit Sermon Media Post'),
		'new_item' => __('New Sermon Media Post'),
		'view_item' => __('View Sermon Media Post'),
		'search_items' => __('Search Sermon Media Posts'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
		);
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
	    '_builtin' => false, // It's a custom post type, not built in!
		'menu_position' => 5,
        'hierarchical' => false,
		'rewrite' => array("slug" => "sermon-media"), // Permalinks
		'register_meta_box_cb' => 'add_cpt_metaboxes',
        'query_var' => true,
'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'comments', 'author', 'custom-fields')
       );

	 $labels2 = array(
		'name' => _x('Staff Members', 'post type general name'),
		'singular_name' => _x('Staff Member', 'post type singular name'),
		'add_new' => _x('Add New', 'Staff Member'),
		'add_new_item' => __('Add New Staff Member'),
		'edit_item' => __('Edit Staff Member'),
		'new_item' => __('New Staff Member'),
		'view_item' => __('View Staff Member'),
		'search_items' => __('Search Staff Members'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
		);
    $stuff = array(
        'labels' => $labels2,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
	    '_builtin' => false, // It's a custom post type, not built in!
		'menu_position' => 5,
        'hierarchical' => true,
		'rewrite' => array("slug" => "staff"), // Permalinks
		'register_meta_box_cb' => 'add_cpt_metaboxes',
        'query_var' => true,
'supports' => array('title', 'editor', 'thumbnail', 'page-attributes')
       );

    if ( $blog_id == 1 ) {
        register_post_type( 'sermonaudio' , $args );
   		register_post_type( 'staff' , $stuff );
   		register_post_type( 'ww' , $wwargs );
   		register_post_type( 'featured' , $featuredargs );
    }
}



// Custom Taxonomies for Sermon Media

add_action( 'init', 'create_sermonaudio_taxonomies', 0 );
function create_sermonaudio_taxonomies()
{
  // New taxonomy, hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Seasons', 'taxonomy general name' ),
    'singular_name' => _x( 'Season', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Seasons' ),
    'all_items' => __( 'All Seasons' ),
    'parent_item' => __( 'Parent Season' ),
    'parent_item_colon' => __( 'Parent Season:' ),
    'edit_item' => __( 'Edit Season' ),
    'update_item' => __( 'Update Season' ),
    'add_new_item' => __( 'Add New Season' ),
    'new_item_name' => __( 'New Season Name' ),
  );

  register_taxonomy('season',array('sermonaudio'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'season' ),
  ));

  // New taxonomy, hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Sermon Series', 'taxonomy general name' ),
    'singular_name' => _x( 'Sermon Series', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Sermon Series' ),
    'all_items' => __( 'All Sermon Series' ),
    'parent_item' => __( 'Parent Sermon Series' ),
    'parent_item_colon' => __( 'Parent Sermon Series:' ),
    'edit_item' => __( 'Edit Sermon Series' ),
    'update_item' => __( 'Update Sermon Series' ),
    'add_new_item' => __( 'Add New Sermon Series' ),
    'new_item_name' => __( 'New Sermon Series Name' ),
  );

  register_taxonomy('series',array('sermonaudio'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'series' ),
  ));

  // New taxonomy, hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Speakers', 'taxonomy general name' ),
    'singular_name' => _x( 'Speaker', 't
