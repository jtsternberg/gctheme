	   <div class="footer1">

		  <ul class="footer2"><strong>Weekend Worship:</strong>
	   		  <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'W' ) ); ?>
		  </ul>
		  <strong class="footer3"><a href="<?php bloginfo('url'); ?>/family-ministry">Family Ministry</a>:  <a href="<?php bloginfo('url'); ?>/small-groups">Small Groups</a>:  <a href="<?php bloginfo('url'); ?>/missions-outreach">Missions &amp; Outreach</a>:</strong>

	   <p class="footer4"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/gc2_18.png" border="0" />&nbsp;copyright (&copy;) 2005 - <?php echo date("Y") ?> <a href="#">generations church</a>. all rights reserved. join us on&nbsp;<a href="https://www.facebook.com/GenerationsChurchNC" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/facebook_18.png" border="0" /></a>&nbsp;<a href="http://twitter.com/generationschch" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/twitter_18.png" width="18" height="18" border="0" /></a>&nbsp;<a href="https://www.flickr.com/GenerationsChurchNC" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/flickr_18.png" width="18" height="18" border="0" /></a>&nbsp;<a href="https://vimeo.com/channels/Generations" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/vimeo_18.png" width="18" height="18" border="0" /></a>&nbsp;<a href="https://www.youtube.com/GenerationsVideos" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/youtube_18.png" width="18" height="18" border="0" /></a></p>


	    </div>


</div>
<?php wp_footer(); ?>
<div id="podPress_footer" style="text-align: center;"><cite>Podcast powered by <a href="http://www.mightyseek.com/podpress/" title="podPress, a plugin for podcasting with WordPress"><strong>podPress (v8.8 / v8.8.6.2)</strong></a></cite></div>
</body>
</html>
