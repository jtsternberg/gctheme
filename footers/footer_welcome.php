	 <div id="links">
		<div class="links">
			<?php wp_nav_menu( array( 'theme_location' => 'home'/*, 'link_after' => ' // '*/ ) ); ?>
			<!-- <a href="<?php bloginfo('url'); ?>/about-us/">Who Are We?</a> // <a href="<?php bloginfo('url'); ?>/about-us/where-and-when/">Where and When?</a> // <a href="<?php bloginfo('url'); ?>/sermon-media/">Messages</a> // <a href="<?php bloginfo('url'); ?>/gc-news/">GC News</a> // <a href="<?php bloginfo('url'); ?>/about-us/contact-us/">Contact Us</a> // <a href="<?php bloginfo('url'); ?>/giving/">Online Giving</a> // <a href="<?php bloginfo('url'); ?>/about-us/contact-us/login/">Member Login</a> -->
		</div>
	 </div>
	 <div id="footerleft">
	   <p class="footer">join us on&nbsp;<a href="https://www.facebook.com/GenerationsChurchNC" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/facebook_18.png" alt="facebook" border="0" /></a>&nbsp;<a href="http://twitter.com/generationschch" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/twitter_18.png" alt="twitter" width="18" height="18" border="0" /></a>&nbsp;<a href="https://www.flickr.com/GenerationsChurchNC" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/flickr_18.png" alt="flickr" width="18" height="18" border="0" /></a>&nbsp;<a href="https://vimeo.com/channels/Generations" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/vimeo_18.png" alt="vimeo" width="18" height="18" border="0" /></a>&nbsp;<a href="https://www.youtube.com/GenerationsVideos" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/youtube_18.png" alt="youtube" width="18" height="18" border="0" /></a></p>
	 </div>

	 <div id="footerright">
	   <p class="footer"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/gc2_18.png" alt="GC Steps Logo" border="0" />&nbsp;Copyright (&copy;) 2005 - <?php echo date("Y") ?> <a href="#">generations church</a>. All Rights Reserved.<br />4874 Long Beach Road SE | Southport, NC 28461 | 910.454.9302</p>
	   <small><?php echo GC_OOP::go()->footer_credits(); ?></small>
	 </div>

</div>
<?php wp_footer(); ?>
</body>
</html>
