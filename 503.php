<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="description" content="<?php bloginfo('description'); ?>" />
	<title><?php bloginfo('name'); ?> &rsaquo; <?php echo $this->g_opt['mamo_pagetitle']; ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

</head>

<body>

<div id="container">
  	<div id="header">

  	  <div id="logo2">
  	  <h1 class="logotext2"><a href="http://generationschurch.com">Generations Church</a></h1>
  	  </div>

  <div id="content" style="width:500px; padding-top:20px;">
  	<div id="centercolumn" style="width:420px; border:1px solid #f8981c; padding:20px; padding-bottom:1PX;">

<h1 style="text-align:center; margin-bottom:20px; pa">Maintenance Mode</h1>

		<form name="loginform" id="loginform" action="<?php echo site_url('wp-login.php', 'login_post') ?>" method="post">
			<?php echo $this->mamo_template_tag_message(); ?>
		</form>

		<p id="nav">
			<?php echo $this->mamo_template_tag_login_logout(); ?>
		</p>
</div>






 <p class="footer4" style="width:420px; margin-bottom:20px;">copyright (&copy;) 2005 - <?php echo date("Y") ?> <a href="#">generations church</a>. all rights reserved.<br /><br />join us on&nbsp;<a href="https://www.facebook.com/GenerationsChurchNC" target="_blank"><img style=" vertical-align:text-bottom;" style=" vertical-align:text-bottom;" src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/facebook_18.png" border="0" /></a>&nbsp;<a href="http://twitter.com/generationschch" target="_blank"><img style=" vertical-align:text-bottom;" src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/twitter_18.png" width="18" height="18" border="0" /></a>&nbsp;<a href="https://www.flickr.com/GenerationsChurchNC" target="_blank"><img style=" vertical-align:text-bottom;" src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/flickr_18.png" width="18" height="18" border="0" /></a>&nbsp;<a href="https://vimeo.com/channels/Generations" target="_blank"><img style=" vertical-align:text-bottom;" src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/vimeo_18.png" width="18" height="18" border="0" /></a>&nbsp;<a href="https://www.youtube.com/GenerationsVideos" target="_blank"><img style=" vertical-align:text-bottom;" src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/youtube_18.png" width="18" height="18" border="0" /></a></p>


</div>
<?php wp_footer(); ?>
</body>
</html>
