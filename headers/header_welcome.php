<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<?php if ( !class_exists( 'All_in_One_SEO_Pack' ) ) : ?>
	<meta name="keywords" content="generations, church, pastor, troy, knight, baptist, southport, oak island, leland, shallotte, supply, brunswick county, north, carolina"/>
	<meta name="description" content="Our vision at Generations Church is to lead 10,000 unchurched people in Brunswick County to become fully devoted followers of Jesus Christ, and our mission is to invite people to take their next step with God."/>

<?php endif; ?>

<meta name="author" content="Justin Sternberg"/>
<meta name="google-site-verification" content="SBtVCTNagK_ptkcBbi_SI2G1wqMhf-L2joigONs0etg" />
<link type="text/plain" rel="author" href="<?php bloginfo('url'); ?>/humans.txt" />

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<!--[if lte IE 7]>
<style>
#menuwrapper, #p7menubar ul a {height: 1%;}
a:active {width: auto;}
</style>
<![endif]-->

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_head(); ?>
</head>
<body>

<div id="container">
  	<div id="header">

  	  <div id="logo">
  	  <h1 class="logotext"><a href="<?php bloginfo('url'); ?>">Generations Church</a></h1>
  	  </div>

		<div class="searchbar">
			<div id="sitemap"><a href="<?php bloginfo('url'); ?>/site-map">site map</a></div>
				<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>
   </div>
