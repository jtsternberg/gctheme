<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/welcome2.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.featureList-1.0.0.js'></script>



<!--[if lte IE 7]>
<style>
#menuwrapper, #p7menubar ul a {height: 1%;}
a:active {width: auto;}
</style>
<![endif]-->

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />


<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

<?php wp_head(); ?>
</head>
<body>

<div id="container">
  	<div id="header">
 
  	  <div id="logo">
  	  <h1 class="logotext"><a href="welcome.html">Generations Church</a></h1>
  	  </div>
			
		<div class="searchbar">
			<div id="sitemap"><a href="#">site map</a>			</div>
			<div id="form">
			<form method="get" action="<?php bloginfo('url'); ?>/">
				<fieldset>
				<input type="text" name="s" value="Search" id="s" class="text" />
				<input name="submit" class="cat_button" src="<?php bloginfo('template_directory'); ?>/images/go.gif" type="image" /> 
				</fieldset>
			</form>
			</div>
		</div>
   </div>  
