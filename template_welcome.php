<?php 
/*
Template Name: Welcome
*/

include('headers/header_welcome.php');
?>
 	 <div id="content">
  	    <div id="navright">
    		 <ul class="navlinks">
      		 <li><a href="<?php bloginfo('url'); ?>/weekend-worship/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/W.png" alt="Worship" class="floatLeft" />Weekend<br />Worship</a></li>
     		 <li><a href="<?php bloginfo('url'); ?>/family-ministry/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/FM.png" alt="Family Ministry" class="floatLeft" />Family<br />Ministry</a></li>
      		 <li><a href="<?php bloginfo('url'); ?>/small-groups/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/SG.png" alt="Small Groups" class="floatLeft" />Small<br />Groups</a></li>
      		 <li><a href="<?php bloginfo('url'); ?>/missions-outreach/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/M.png" alt="Missions &amp; Outreach" class="floatLeft" />Missions<br />&amp;Outreach</a></li>
     		 </ul>
  	    </div>
		
		<div id="feature_wrapper">
				<div id="feature_list">
				
					<ul id="tabs">
						<li class="one">
							<a href="javascript:;"><span class="featuretitle">						
							<?php $my_query = new WP_Query('post_type=featured&ministry=family-ministry&showposts=1'); while ($my_query->have_posts()) : $my_query->the_post(); the_excerpt();  endwhile; ?> 
							</span></a></li>
						<li class="two">
							<a href="javascript:;"><span class="featuretitle">						
							<?php $my_query = new WP_Query('post_type=featured&ministry=small-groups&showposts=1'); while ($my_query->have_posts()) : $my_query->the_post(); the_excerpt();  endwhile; ?> 
							</span></a></li>
						<li class="three">
							<a href="javascript:;"><span class="featuretitle">						
							<?php $my_query = new WP_Query('post_type=featured&ministry=missions-outreach&showposts=1'); while ($my_query->have_posts()) : $my_query->the_post(); the_excerpt();  endwhile; ?> 
							</span></a></li>
						
					</ul>


					<ul id="output">
						<li>
							<?php 
							$my_query = new WP_Query('post_type=ww&ministry=family-ministry&showposts=1'); while ($my_query->have_posts()) : $my_query->the_post(); 
							$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'current-series');
							$linkbox = get_post_meta($post->ID, 'link', true);
							if ($linkbox == '') {
								$linkbox = get_bloginfo('url'). '/family-ministry/';
							} 
							?>
							<a id="fmoutput" href="<?php echo $linkbox; ?>"><img src="<?php echo $thumbnail[0]; ?>"/></a>
							<?php endwhile; ?>
						</li>
						<li>
							<?php 
							$my_query = new WP_Query('post_type=ww&ministry=small-groups&showposts=1'); while ($my_query->have_posts()) : $my_query->the_post(); 
							$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'current-series');
							$linkbox = get_post_meta($post->ID, 'link', true);
							if ($linkbox == '') {
								$linkbox = get_bloginfo('url'). '/small-groups/';
							} 
							?>
							<a id="sgoutput" href="<?php echo $linkbox; ?>"><img src="<?php echo $thumbnail[0]; ?>"/></a>
							<?php endwhile; ?>
						</li>
						<li>
							<?php 
							$my_query = new WP_Query('post_type=ww&ministry=missions-outreach&showposts=1'); while ($my_query->have_posts()) : $my_query->the_post(); 
							$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'current-series');
							$linkbox = get_post_meta($post->ID, 'link', true);
							if ($linkbox == '') {
								$linkbox = get_bloginfo('url'). '/missions-outreach/';
							} 
							?>
							<a id="croutput" href="<?php echo $linkbox; ?>"><img src="<?php echo $thumbnail[0]; ?>"/></a>
							<?php endwhile; ?>
						</li>
						
						<li>
							<?php 
							$my_query = new WP_Query('post_type=ww&ministry=weekend-worship&showposts=1'); while ($my_query->have_posts()) : $my_query->the_post(); 
							$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'current-series');
							$linkbox = get_post_meta($post->ID, 'link', true);
							$actualcolornum = str_replace( 'gc-color-', '', get_post_meta($post->ID, 'textcolor-meta', true) );

							if ($linkbox == '') {
								$linkbox = get_bloginfo('url'). '/weekend-worship/';
							} 
							?>
							<a id="woutput" href="<?php echo $linkbox; ?>"><img src="<?php echo $thumbnail[0]; ?>"/>
								<span class="weekend" style="color: #<?php echo $actualcolornum; ?>">
								<?php the_title(); ?>
								<?php the_content(); ?>
								</span>
							</a>
							<?php endwhile; ?>
						</li>

					</ul>
				</div><!-- // Feature list -->
		</div><!-- // Feature wrapper -->




 	 </div>

<?php include('footers/footer_welcome.php');?>



