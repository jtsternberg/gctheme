<?php get_header(); ?>
<?php include('headers/WW-header.php');?>

  <div id="content">
  	<div id="leftcolumn">
<h1>Sermon Media Archives</h1>
<br />
<p>If you would like to subscribe to automatically receive sermon Media from GC, <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewPodcast?id=327415157">please click here</a>. Make sure you have <a onclick="window.open(this.href); return false;" onkeypress="window.open(this.href); return false;" href="http://www.apple.com/itunes/download" title="http://www.apple.com/itunes/download">iTunes</a> before you subscribe (it&#39;s free). You may also subscribe to the <a href="http://www.generationschurch.com/podcast/feed/">RSS feed</a>.</p>

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<small><?php the_time('F jS, Y') ?> <!-- by <?php the_author() ?> --></small>

				<div class="entry">
				<P><?php the_excerpt() ?></P>
					<?php the_content('Read the rest of this entry &raquo;'); ?>
				</div>

				<?php comments_template(); ?>

				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &rarr;', '1 Comment &rarr;', '% Comments &rarr;'); ?></p>
			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignright"><?php next_post_link('%link', 'Newer Post &rarr;'); ?></div>
			<div class="alignleft"><?php previous_post_link('%link', '&larr; Older Post'); ?></div>

		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>


	<?php endif; ?>


  	</div>
<?php include('sidebar/sidebar_sermon_Media.php');?>
<?php get_footer(); ?>



