<?php
get_header();
$in_sg_questions = in_category( 'small-group-questions' );
$suffix = $in_sg_questions ? 'SG' : 'WW';
include("headers/$suffix-header.php");
?>

  <div id="content">
  	<div id="leftcolumn">

	<?php if (have_posts()) :
		while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h1><?php the_title(); ?></h1>
				<p><?php
				the_time('F jS, Y');
				if ( !$in_sg_questions ) {
					echo ' by '; the_author_link();
				}

				echo ' | ';
				if ( function_exists('wp_print') )
					print_link();
				echo get_the_term_list( get_the_ID(), 'series', ' | Series: ', ', ', '' );
				?>
				</p>

				<div class="entry">
					<?php the_post_thumbnail( 'staff' );
					the_content('Read the rest of this entry &raquo;'); ?>
				</div>
				<?php if( function_exists('ADDTOANY_SHARE_SAVE_KIT') ) { ADDTOANY_SHARE_SAVE_KIT(); }
				if ( get_post_meta($post->ID, 'hide-comments', true) == 'yes' ) { ?><br /><br /><p>Comments closed on this post.</p><?php }
				else { comments_template();	} ?>

				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &rarr;', '1 Comment &rarr;', '% Comments &rarr;'); ?></p>
			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignright"><?php next_post_link('%link', 'Newer Post &rarr;'); ?></div>
			<div class="alignleft"><?php previous_post_link('%link', '&larr; Older Post'); ?></div>

		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>

<?php endif; ?>


  	</div>
<div id="rightcolumn">

<?php
if ( $in_sg_questions )
	include('sidebars/SG-sidebar.php');
else
	include('sidebars/gc-news.php');
?>

</div>

<?php
if ( $in_sg_questions )
		get_footer();
else
    include('footers/gc-news-footer.php');
