	   <div class="footer1">
<?php
if ( is_tree('19') ) { ?>
		  <ul class="footer2"><strong>Weekend Worship:</strong>
	   		  <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'W' ) ); ?>
		  </ul>
		  <strong class="footer3"><a href="<?php bloginfo('url'); ?>/family-ministry">Family Ministry</a>:  <a href="<?php bloginfo('url'); ?>/small-groups">Small Groups</a>:  <a href="<?php bloginfo('url'); ?>/missions-outreach">Missions &amp; Outreach</a>:</strong><?php


} elseif ( is_tree('21') ) { ?>
		  <ul class="footer2"><strong>Small Groups:</strong>
	   		  <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'SG' ) ); ?>
		  </ul>
		  <strong class="footer3"><a href="<?php bloginfo('url'); ?>/weekend-worship">Weekend Worship</a>:  <a href="<?php bloginfo('url'); ?>/family-ministry">Family Ministry</a>:  <a href="<?php bloginfo('url'); ?>/missions-outreach">Missions &amp; Outreach</a>:</strong><?php


} elseif ( is_tree('20') ) { ?>
		  <ul class="footer2"><strong>Family Ministry:</strong>
	   		  <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'FM' ) ); ?>
		  </ul>
		  <strong class="footer3"><a href="<?php bloginfo('url'); ?>/weekend-worship">Weekend Worship</a>:  <a href="<?php bloginfo('url'); ?>/small-groups">Small Groups</a>:  <a href="<?php bloginfo('url'); ?>/missions-outreach">Missions &amp; Outreach</a>:</strong><?php


} elseif ( is_tree('2703') ) {?>
		  <ul class="footer2"><strong>Missions &amp; Outreach:</strong>
	   		  <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'MO' ) ); ?>
		  </ul>
		  <strong class="footer3"><a href="<?php bloginfo('url'); ?>/weekend-worship">Weekend Worship</a>:  <a href="<?php bloginfo('url'); ?>/family-ministry">Family Ministry</a>:  <a href="<?php bloginfo('url'); ?>/small-groups">Small Groups</a>:</strong><?php


} else { ?>
		  <strong><a href="<?php bloginfo('url'); ?>/weekend-worship">Weekend Worships</a>: &nbsp;<a href="<?php bloginfo('url'); ?>/family-ministry">Family Ministry</a>: &nbsp;<a href="<?php bloginfo('url'); ?>/small-groups">Small Groups</a>: &nbsp;<a href="<?php bloginfo('url'); ?>/missions-outreach">Missions &amp; Outreach</a>:</strong><?php
} ?>


 <p class="footer4"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/gc2_18.png" border="0" />&nbsp;copyright (&copy;) 2005 - <?php echo date("Y") ?> <a href="#">generations church</a>. all rights reserved. join us on&nbsp;<a href="https://www.facebook.com/GenerationsChurchNC" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/facebook_18.png" border="0" /></a>&nbsp;<a href="http://twitter.com/generationschch" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/twitter_18.png" width="18" height="18" border="0" /></a>&nbsp;<a href="https://www.flickr.com/GenerationsChurchNC" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/flickr_18.png" width="18" height="18" border="0" /></a>&nbsp;<a href="https://vimeo.com/channels/Generations" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/vimeo_18.png" width="18" height="18" border="0" /></a>&nbsp;<a href="https://www.youtube.com/GenerationsVideos" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/SocialMediaIcons/youtube_18.png" width="18" height="18" border="0" /></a><br /><?php echo GC_OOP::go()->footer_credits(); ?></p>
	    </div>


</div>
<?php wp_footer(); ?>
</body>
</html>
