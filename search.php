<?php get_header();

include('headers/alt-header.php');
?>


  <div id="content">
  	<div id="leftcolumn">

	<?php if (have_posts()) : ?>

				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyten' ), '<span>' . get_search_query() . '</span>' ); ?></h1>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
		</div>
		<div style="clear:both; margin:0; padding:0;"></div>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?>>
				<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
				<p><?php the_excerpt('') ?></p><br />

		</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
		</div>
		<div style="clear:both; margin:0; padding:0;"></div>

		<div class="searchbar2"><?php get_template_part( 'repeat_elements/searchform' ); ?>
</div>



	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>


		<div class="searchbar2">				<?php get_template_part( 'repeat_elements/searchform' ); ?>
</div>


	<?php endif; ?>
</div>

<div id="rightcolumn">


<?php
    include('sidebars/alt-sidebar.php');
?>

</div>

<?php get_footer(); ?>


