<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="keywords" content="generations, church, pastor, troy, knight, baptist, southport, oak island, leland, shallotte, supply, brunswick county, north, carolina"/>

<meta name="author" content="Justin Sternberg"/>
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<title><?php wp_title('&larr;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="container">
	<div id="header">

		<div id="logo">
			<h1 class="logotext"><a href="<?php echo site_url(); ?>">Generations Church</a></h1>
		</div>

		<div id="topNav">
			<ul class="topMid">
				<?php global $blog_id; if ( $blog_id != 1 ) switch_to_blog(1);
				wp_nav_menu( array( 'theme_location' => 'top' ) );
				restore_current_blog();
				?>
			</ul>
			<div class="searchbar">
				<div id="sitemap"><a href="<?php echo site_url(); ?>/site-map">site map</a></div>
					<?php get_template_part( 'repeat_elements/searchform' ); ?>
			</div>
		</div>
	</div>
