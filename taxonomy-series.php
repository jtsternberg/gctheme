
<?php get_header(); ?>

<?php
	include('headers/WW-header.php');
?>

  <div id="content">
  	<div id="leftcolumn">

		<?php

	 if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<?php
			$pre = '';
			if ( get_post_type() == 'post' )
				$pre = get_the_category_list( ', ' ) . ': ';
			elseif ( get_post_type() == 'sermonaudio' )
				$pre = '<a href="'. site_url( '/sermon-media' ) .'">Message Archive</a>: ';
			?>
				<h2 class="posts series-archive-title"><?php echo $pre; ?><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<small>
					<?php the_time('F jS, Y');
					if ( $speaker = get_the_term_list($post->ID,  'speaker', '', ', ', '') ) {
						echo ' by '. $speaker;
					}
					if ( $series = get_the_term_list($post->ID,  'series', '', ', ', '') ) {
						echo ' in the series '. $series;
					}
					?>
				</small>

				<div class="entry">

					<?php
					echo '<a href="', the_permalink(), '">', the_post_thumbnail( 'staff' ), '</a>';
					?>
					<?php the_content('Read the rest of this entry &raquo;'); ?>
				</div>

				<p class="postmetadata">topics: <?php echo get_the_term_list($post->ID,  'topic', '', ', ', ''); ?>
				<br /></p><?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &rarr;', '1 Comment &rarr;', '% Comments &rarr;'); ?></p>

			</div>
		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>

<?php endif;
wp_reset_query();
?>



  	</div>
<div id="rightcolumn">

<?php
	include('sidebars/gc-news.php');
?>

</div>

<?php get_footer(); ?>
