<?php get_header(); ?>

<?php
	include('headers/WW-header.php');
?>

  <div id="content">
  	<div id="leftcolumn">
<h1>Sermon Audio</h1>
<p>If you would like to subscribe to automatically receive podcasts from GC, <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewPodcast?id=327415157">please click here.</a>  Make sure you have <a href="http://www.apple.com/itunes/download">iTunes</a> before you subscribe (it's free). You may also subscribe to the <a href="<?php bloginfo('url'); ?>/feed/?post_type=sermon-audio">RSS feed</a>.</p>

		<?php

	 if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2 class="posts"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<small><?php the_time('F jS, Y') ?> by <?php echo get_the_term_list($post->ID,  'speaker', '', ', ', ''); ?> in the series "<?php echo get_the_term_list($post->ID,  'series', '', ', ', ''); ?>"</small>

				<div class="entry">

					<?php the_post_thumbnail( 'series-art' ); ?>
					<?php the_content('Read the rest of this entry &raquo;'); ?>
					<?php the_excerpt() ?>
				</div>

				<p class="postmetadata">topics: <?php echo get_the_term_list($post->ID,  'topic', '', ', ', ''); ?>
				<br /></p><?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('Click to add comment &#187;', '1 Comment, click to view &#187;', '% Comments, click to view &#187;'); ?></p>

			</div>
		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif;
wp_reset_query();
?>



  	</div>
<div id="rightcolumn">

<?php
	include('sidebars/sermon-audio.php');
?>

</div>

<?php
    include('footers/sermon-audio-footer.php'); 
?>
