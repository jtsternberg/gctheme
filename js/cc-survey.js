// Copyright 2008 by Constant Contact Inc.,
var ConstantContactPoll_a07e33yw3srgg2wptbt = function() {
  var pollHtml = '<div class="textwidget survey">Do you consider GC your church home?<form name="formname"><label><input onclick="ConstantContactPoll_a07e33yw3srgg2wptbt.selectAnswer( this.value)" value="0" name="cc-poll-answer-option_a07e33yw3srgg2wptbt" type="radio"/> Yes </label><label><input onclick="ConstantContactPoll_a07e33yw3srgg2wptbt.selectAnswer( this.value)" value="1" name="cc-poll-answer-option_a07e33yw3srgg2wptbt" type="radio"/> No </label><label><input onclick="ConstantContactPoll_a07e33yw3srgg2wptbt.selectAnswer( this.value)" value="2" name="cc-poll-answer-option_a07e33yw3srgg2wptbt" type="radio"/> I need more information </label><label class="buttons"><button onclick="ConstantContactPoll_a07e33yw3srgg2wptbt.submit()" value="Vote" name="vote" type="button">Submit</button></label></form></div>';
  var pollResultsHtml = '<div style="  border-bottom: 1px solid #b7b7b7;   padding: 0.5em;   font-family: Arial;  font-size: 12pt;  color: #000000;">Do you consider GC your church home?</div><div style="  border-bottom: 1px solid #b7b7b7;   border-top: 1px solid #FFFFFF;   padding: 0.5em;   font-family: Arial;   font-size: 10pt;   color: #000000;">Yes<div><img src="http://img.constantcontact.com/ui/images/spacer.gif" width="100px" height="20px;" border="0"  style="background-color: orange;" valign="bottom" />\n      100%\n    </div></div><div style="  border-bottom: 1px solid #b7b7b7;   border-top: 1px solid #FFFFFF;   padding: 0.5em;   font-family: Arial;   font-size: 10pt;   color: #000000;">No<div><img src="http://img.constantcontact.com/ui/images/spacer.gif" width="0px" height="20px;" border="0"  style="background-color: orange;" valign="bottom" />\n      0%\n    </div></div><div style="  border-bottom: 1px solid #b7b7b7;   border-top: 1px solid #FFFFFF;   padding: 0.5em;   font-family: Arial;   font-size: 10pt;   color: #000000;">I need more information<div><img src="http://img.constantcontact.com/ui/images/spacer.gif" width="0px" height="20px;" border="0"  style="background-color: orange;" valign="bottom" />\n      0%\n    </div></div>';

  var instanceId = "a07e33yw3srgg2wptbt";
  var pollSubmitUrl = "http://survey.constantcontact.com/poll/a07e33yw3srgg2wptbt/vote.js";
  var selectedOption = -1;

  function doSubmit() {
    var scriptElm = document.createElement("script");
    scriptElm.type = "text/javascript";
    if (pollSubmitUrl.indexOf('?') == -1) {
      pollSubmitUrl += '?';
    }
    scriptElm.src = pollSubmitUrl + "&selectedOption=" + selectedOption + "&instanceId=" + instanceId;

    document.getElementsByTagName("body").item(0).appendChild( scriptElm);
  }

  return {
      showPoll: function() {
        document.write( pollHtml);
      },

      submit: function() {
        if (selectedOption != -1) {
          doSubmit();
        } else {
          this.showResults( pollResultsHtml);
        }
      },

      showResults: function( html) {
        document.getElementById("cc-poll-" + instanceId + "-replace").innerHTML = html;
      },

      selectAnswer: function(i) {
        selectedOption = i;
      }
  }
}();

ConstantContactPoll_a07e33yw3srgg2wptbt.showPoll();