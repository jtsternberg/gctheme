<?php get_header();

if (is_tax(array('series', 'speaker', 'topic'))) {
	include('headers/sermon-media.php');
	} else {
	include('headers/WW-header.php');
	}
?>

  <div id="content">
  	<div id="leftcolumn">
	<h1
		<?php if (!is_tax(array('series', 'speaker', 'topic'))) {
		?>class="staff"<?php
		} ?>
	>"<? echo get_query_var('term'); ?>" <?php echo get_query_var('taxonomy'); ?> Archive</h1>


<?php

	 if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>


			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2 class="posts"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<hr />
				<?php if (is_tax(array('series', 'speaker', 'topic'))) { ?>
				<small><?php the_time('F jS, Y') ?> by <?php echo get_the_term_list($post->ID,  'speaker', '', ', ', ''); ?> in the series "<?php echo get_the_term_list($post->ID,  'series', '', ', ', ''); ?>"</small>
				<?php } ?>
				<?php echo get_the_term_list($post->ID,  'position', '<h3>- ', ', ', '</h3>'); ?>

				<div class="entry">


					<a class="bodylink" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
					<?php if (is_tax(array('series', 'speaker', 'topic'))) {
						if ( !has_excerpt())
						{ echo ''; }
						else { ?><h3 class="gc_excerpt"><em><?php the_excerpt(); ?></em></h3><?php
						}
					the_post_thumbnail( 'series-art' );
					the_content('Read the rest of this entry &rarr;');
						if (function_exists('ADDTOANY_SHARE_SAVE_KIT')) {
						ADDTOANY_SHARE_SAVE_KIT();
						}
					} else {
					the_post_thumbnail( 'gc-news' );
					the_excerpt();
					} ?>
					</a>
				</div>
				<p class="postmetadata">
					<?php if (is_tax(array('series', 'speaker', 'topic'))) {
					echo get_the_term_list($post->ID,  'topic', 'Topics: ', ', ', '');  ?>
					<br />
					<?php edit_post_link('Edit', '', ' | '); ?>
					<?php comments_popup_link('Click to add comment &rarr;', '1 Comment, click to view &rarr;', '% Comments, click to view &rarr;');
					} else { edit_post_link('Edit', '', ' | '); } ?>
				</p>
			</div>
			<hr class="bottomhr" />
		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>

<?php endif;
wp_reset_query();
?>



  	</div>
<div id="rightcolumn">


<?php
	if (is_tax(array('series', 'speaker', 'topic'))) {
	include('sidebars/sermon-media.php');
	} else {
	/* ?>
		<h2>Staff Positions</h2><br />
		<?php $my_query = new WP_Query( array('post_type' => staff, 'posts_per_page' => 50, 'orderby' => menu_order, 'order' => ASC) ); while ($my_query->have_posts()) : $my_query->the_post(); ?>
		<h3 style="margin-top:5px;"><?php $terms_as_text = get_the_term_list( $post->ID, 'position', '', ', ', '' ) ;echo strip_tags($terms_as_text); ?></h3>
		<a href="<?php the_permalink(); ?>"><?php the_title('', '');?></a>
		<?php endwhile;
	*/
		$taxonomyname = get_query_var('taxonomy');
		echo '<h3>All ' . $taxonomyname .'</h3><br/>';
		$terms = get_terms($taxonomyname);
		echo "<ul>";
		foreach ($terms as $term) {
			echo "<li><a href='". site_url();
				if (is_tax('foods')) {
				echo '/staff-favorite-foods/';
				} elseif (is_tax('position')) {
				echo '/staff-position/';
				} elseif (is_tax('books')) {
				echo '/staff-favorite-books/';
				} elseif (is_tax('movies')) {
				echo '/staff-favorite-movies/';
				} elseif (is_tax('artists')) {
				echo '/staff-favorite-artists/';
				} elseif (is_tax('activities')) {
				echo '/staff-favorite-activities/';
				} elseif (is_tax('verses')) {
				echo '/staff-favorite-verses/';
				} elseif (is_tax('favorite-things')) {
				echo '/staff-favorite-things/';
				}

			echo $term->slug. "'>" .$term->name. "</a></li>";
		}
		echo "</ul>";

	}
?>

</div>

<?php
    include('footers/sermon-media-footer.php');
?>
