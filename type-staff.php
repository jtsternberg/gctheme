<?php

get_header();

	include('headers/WW-header.php');
?>

  <div id="content">
  	<div id="leftcolumn">
<h1>Meet The Staff</h1>



<?php
query_posts( array('post_type' => staff,  'paged' => $paged) );

	 if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>


			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<hr />
				<h3><a href="<?php the_permalink(); ?>"><?php $terms_as_text = get_the_term_list( $post->ID, 'position', '- ', ', ', '' ) ;echo strip_tags($terms_as_text); ?></a></h3>

				<div class="entry">


					<a class="bodylink" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
					<?php the_post_thumbnail( 'staff' ); ?>
					<?php the_excerpt() ?>
					</a>
				</div>
				<p class="postmetadata">
				<?php edit_post_link('Edit', '', ' | '); ?>

			</div>
				<hr class="bottomhr" />
		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>

<?php endif;
wp_reset_query();
?>



  	</div>
<div id="rightcolumn">

<div class="staff">
<?php
if(has_position_tag(null)) {

include('sidebars/staff.php'); }

else { ?>
 <h2>Staff Positions</h2>
<?php $my_query = new WP_Query('post_type=staff'); while ($my_query->have_posts()) : $my_query->the_post();
echo get_the_term_list($post->ID, 'position', '', ', ','');

endwhile;
}
?>
</div>

</div>

<?php
    include('footers/sermon-media-footer.php');
?>
