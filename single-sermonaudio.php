<?php get_header(); ?>

<?php
	include('headers/sermon-media.php');
?>

  <div id="content">
  	<div id="leftcolumn">

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h1><?php the_title(); ?></h1>
				<small><?php the_time('F jS, Y') ?> by <?php echo get_the_term_list($post->ID,  'speaker', '', ', ', ''); ?> in the series "<?php echo get_the_term_list($post->ID,  'series', '', ', ', ''); ?>"</small>

				<div class="entry">
					<h3 class="gc_excerpt"><em>
					<?php
						if ( !has_excerpt())
						{ echo ''; }
						else { the_excerpt(); }
					?>
					</em></h3>
					<?php the_post_thumbnail( 'series-art' );
					if (!the_content()){ ?><p></p><?php

						}
					else { the_content('Read the rest of this entry &raquo;'); } ?>
				</div><?php if( function_exists('ADDTOANY_SHARE_SAVE_KIT') ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>
				<p class="postmetadata">
				<?php if(has_topic_tag(null)) { ?>
				Topics: <?php echo get_the_term_list($post->ID,  'topic', '', ', ', ''); ?>
				<?php }
				else { echo 'No Topics Listed';}
				?>
				<br /><?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &rarr;', '1 Comment &rarr;', '% Comments &rarr;'); ?></p>
				<?php comments_template(); ?>


			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignright"><?php next_post_link('%link', 'Newer Post &rarr;'); ?></div>
			<div class="alignleft"><?php previous_post_link('%link', '&larr; Older Post'); ?></div>

		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>

<?php endif; ?>


  	</div>
<div id="rightcolumn">

<?php
	include('sidebars/sermon-media.php');
?>

</div>

<?php
    include('footers/sermon-media-footer.php');
?>
