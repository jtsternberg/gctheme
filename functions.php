<?php
class GC_OOP {

	// A single instance of this class.
	public static $instance = null;

	/**
	 * Creates or returns an instance of this class.
	 * @since  0.1.0
	 * @return GC_OOP A single instance of this class.
	 */
	public static function go() {
		if ( self::$instance === null )
			self::$instance = new self();

		return self::$instance;
	}

	public function __construct() {
		$GLOBALS['content_width'] = '699';
		add_filter( 'after_setup_theme', array( $this, 'multisite_dummy_functions' ) );

		add_filter( 'the_content', array( $this, 'filter_vimeo_stuff' ) );
		// Add post name to post class
		add_filter( 'body_class', array( $this, 'body_class' ) );
		// if home, run custom tabs css
		add_action( 'wp_head', array( $this, 'welcome_css' ) );
		// Enqueue our scripts/styles
 		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts_styles' ) );
		// Google Analytics in footer
		add_action( 'wp_footer', array( $this, 'googleanalytics' ) );
		// Custom Login Page
		add_action( 'login_head', array( $this, 'custom_login' ) );
		add_action( 'widgets_init', array( $this, 'widgets_init' ) );
		// change footer
		add_filter( 'admin_footer_text', array( $this, 'remove_footer_admin' ) );
		// gravatar default
		add_filter( 'avatar_defaults', array( $this, 'newgravatar' ) );

		// hook the administrative header logo
		add_action( 'wp_head', array( $this, 'custom_logo' ) );
		add_action( 'admin_head', array( $this, 'custom_logo' ) );
		// favicon
		add_action( 'wp_head', array( $this, 'favicon' ) );
		// remove user registration fields
		add_filter( 'user_contactmethods', array( $this, 'profile_fields' )  );
		// Add Staff Page link
		add_action( 'show_user_profile', array( $this, 'show_extra_profile_fields' ) );
		add_action( 'edit_user_profile', array( $this, 'show_extra_profile_fields' ) );

		add_action( 'personal_options_update', array( $this, 'save_extra_profile_fields' ) );
		add_action( 'edit_user_profile_update', array( $this, 'save_extra_profile_fields' ) );
		// guest-author custom field
		add_filter( 'the_author', array( $this, 'guest_author_name' ) );
		add_filter( 'get_the_author_display_name', array( $this, 'guest_author_name' ) );

		add_filter( 'tablepress_use_default_css', '__return_false' );
		remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );
		add_filter( 'get_the_excerpt', array( $this, 'trim_excerpt' ) );

		// enables custom styles in post editor
		add_editor_style();
		// Add shortcodes to excerpts
		// add_filter( 'dsgnwrks_bible_service', array( $this, 'youversion' ) );

		add_action( 'init', array( $this, 'init' ) );
		add_filter( 'gform_pre_render_2', array( $this, 'tax_dropdown' ) );
	}

	public function multisite_dummy_functions( $args ) {
		if ( ! function_exists( 'switch_to_blog' ) ) {
			function switch_to_blog() {
				return;
			}
		}
		if ( ! function_exists( 'restore_current_blog' ) ) {
			function restore_current_blog() {
				return;
			}
		}
	}

	public function init() {
		$sidebars = array(
			'w' => 'Weekend Worship',
			'fm' => 'Family Ministry',
			'sg' => 'Small Groups',
			'mo' => 'Missions &amp; Outreach',
			'sa' => 'Sidebar Alternate',
			'sm' => 'Sermon Media',
			'gc' => 'GC News',
		);
		foreach ( $sidebars as $id => $sidebar ) {
			register_sidebar( array(
				'name' => $sidebar,
				'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="widget-wrap">',
				'after_widget'  => "</div></div>\n",
				'before_title' => '<h2>',
				'after_title' => '</h2>',
				'id' => $id,
			) );
		}
	}

	public function youversion() { return __FUNCTION__; }

	public function trim_excerpt($text) {
		$raw_excerpt = $text;
		if ( '' == $text ) {
			$text = get_the_content('');

			$text = do_shortcode( $text ); // CHANGED HERE

			$text = apply_filters('the_content', $text);
			$text = str_replace(']]>', ']]>', $text);
			$text = strip_tags($text);
			$excerpt_length = apply_filters('excerpt_length', 55);
			$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
			$words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
			if ( count($words) > $excerpt_length ) {
				array_pop($words);
				$text = implode(' ', $words);
				$text = $text . $excerpt_more;
			} else {
				$text = implode(' ', $words);
			}
		}
		return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
	}

	// Add the unfiltered_html capability back in to WordPress 3.0 multisite.
	public function unfilter_multisite( $caps, $cap ) {
		if ( $cap == 'unfiltered_html' ) {
			unset( $caps );
			$caps[] = $cap;
		}
		return $caps;
	}

	public function filter_vimeo_stuff( $content ) {
		if ( get_post_type() != 'sermonaudio' )
			return $content;
		if ( strpos( $content, ') to view message video' ) )
			return $content;

		if ( !strpos( $content, 'vimeo.com' ) )
			return $content;

		$content = '
		Click play (►) to view message video
		'."\n".
		'<div class="vimeostyle">'. $content .'</div>'."\n";

		return $content;

	}

	public function body_class( $classes ){
		global $post;

		if ( is_tree( 'weekend-worship' ) ) {
			$classes[] = $post->post_name.'-body';
		} elseif ( is_tree( 'missions-outreach' ) ) {
			$classes[] = $post->post_name.'-body';
		} elseif ( is_tree('small-groups') ) {
			$classes[] = $post->post_name.'-body';
		} elseif ( is_tree( 'family-ministry' ) ) {
			$classes[] = $post->post_name.'-body';
		}

		return $classes;
	}

	public function welcome_css() {
		global $blog_id;
		if ( is_front_page() && $blog_id == 1 )
			include('repeat_elements/tabscss.php');
	}

	public function enqueue_scripts_styles() {
		global $blog_id;
		wp_enqueue_style( 'gc-style', get_stylesheet_uri(), null, '1.0.2' );

		if ( is_front_page() && $blog_id == 1 ) {
			wp_enqueue_style( 'gc-style-home', get_template_directory_uri() .'/welcome.css', array( 'gc-style' ), '1.0.1' );
			wp_enqueue_script( 'featureList', get_template_directory_uri() .'/js/jquery.featureList-1.0.0.js', array( 'jquery' ), '1.0.1' );
		} elseif ( 'sermonaudio' == get_post_type() )  {

			wp_enqueue_script( 'qtp_poster', 'http://www.apple.com/library/quicktime/2.0/scripts/qtp_poster.js', array( 'prototype' ) );
			wp_enqueue_style( 'qtp_poster', 'http://www.apple.com/library/quicktime/2.0/stylesheets/qtp_poster.css' );

		}
		if ( is_singular() )
			wp_enqueue_script( 'comment-reply' );

		wp_enqueue_style( 'gc-print-style', get_template_directory_uri() .'/css/print.css', array( 'gc-style' ), '1.0.1', 'print' );
	}


	public function googleanalytics() { ?>
		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-8193540-14']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
	<?php }

	public function custom_login() {
		wp_enqueue_style( 'gc-login', get_stylesheet_directory_uri(). '/custom-login.css' );
	}

	public function widgets_init() { register_widget( 'ConstantContact' ); }

	public function remove_footer_admin () {
		echo 'Fueled by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | Site created by <a href="http://dsgnwrks.pro" target="_blank">DsgnWrks</a>';
	}

	public function newgravatar ($avatar_defaults) {
		$myavatar = get_bloginfo('template_directory') . '/images/steps.gif';
		$avatar_defaults[$myavatar] = "GC";
		return $avatar_defaults;
	}

	public function custom_logo() {
		echo '
		<style type="text/css">
		#header-logo { background-image: url('.get_bloginfo('template_directory').'/images/admin_logo_steps.gif) !important; }
		#wpadminbar .quicklinks li#wp-admin-bar-my-account-with-avatar > a img {width:16px; height 16px;}
		</style>
		';
	}

	public function favicon() {
		echo '<link rel="Shortcut Icon" type="image/x-icon" href="'. get_stylesheet_directory_uri() .'/images/favicon.ico" />';
	}

	public function profile_fields( $contactmethods ) {
		unset($contactmethods['aim']);
		unset($contactmethods['jabber']);
		unset($contactmethods['yim']);
		// Add Twitter
		$contactmethods['twitter'] = 'Twitter';
		//add Facebook
		$contactmethods['facebook'] = 'Facebook';

		return $contactmethods;
	}

	public function show_extra_profile_fields( $user ) { ?>

		<h3>Important profile information</h3>

		<table class="form-table">

			<tr>
				<th><label for="staff">Staff Page Link</label></th>

				<td>
					<input type="text" name="staff" id="staff" value="<?php echo esc_attr( get_the_author_meta( 'staff', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description">Please write your first and last name, no caps with a dash (-) inbetween.  <br />e.g. "first-last", "bob-smith"</span>
				</td>
			</tr>

		</table>
	<?php }

	public function save_extra_profile_fields( $user_id ) {

		if ( !current_user_can( 'edit_user', $user_id ) )
			return false;
		update_usermeta( $user_id, 'staff', $_POST['staff'] );
	}

	public function guest_author_name( $name ) {
		global $post;

		$author = get_post_meta( $post->ID, 'guest-author', true );

		if ( $author )
		$name = $author;

		return $name;
	}

	public function tax_dropdown( $form ) {

		foreach( $form['fields'] as &$field ) {

			if ( $field['type'] != 'select' || strpos( $field['cssClass'], 'sermon-dropdown' ) === false)
				continue;

			// you can add additional parameters here to alter the posts that are retreieved
			// more info: http://codex.wordpress.org/Template_Tags/get_posts
			$posts = get_posts( 'numberposts=-1&post_status=publish');

			$args = array (
				'numberposts' => 5,
				'orderby' => 'meta_value_num',
				'order' => 'DESC',
				'post_type' => 'sermons',
				'post_status' => 'publish',
				'meta_key' => '_gc_sermon_date',
			);
			$sermon_posts = get_posts( $args );
			// update 'Select a Post' to whatever you'd like the instructive option to be
			$choices = array( array( 'text' => 'Select Sermon', 'value' => '' ) );

			foreach( $sermon_posts as $sermon ) {
				$choices[] = array( 'text' => $sermon->post_title, 'value' => $sermon->post_title ) ;
			}

			$field['choices'] = $choices;

		}

		return $form;
	}

	function footer_credits() {
		echo 'Site by <a class="u" href="http://dsgnwrks.pro" target="_blank">DsgnWrks</a>.';
	}

}
GC_OOP::go();

// Constant Contact Widget
class ConstantContact extends WP_Widget {
	/** constructor */
	function ConstantContact() {
		parent::WP_Widget(false, $name = 'Constant Contact');
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance) {
		extract( $args );
		$title = apply_filters('widget_title', $instance['title']);
		?>
			  <?php echo $before_widget; ?>
				  <?php if ( $title )
						echo $before_title . $title . $after_title; ?>
<div id="wp_email_capture" class="textwidget">
 If you would like to receive the GC News Newsletter

<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post">
<input type="hidden" name="llr" value="584xm7bab">
<input type="hidden" name="m" value="1101538262910">
<input type="hidden" name="p" value="oi">


<label class="wp-email-capture-email">Email:</label>
<input name="ea" type="text" class="wp-email-capture-email"><br/>

<label class="buttons">
<button name="go" type="submit" value="Go">Submit</button></label>

</form>
</div>
			  <?php echo $after_widget; ?>
		<?php
	}

	/** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['title'] = strip_tags($new_instance['title']);
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance) {
		$title = esc_attr($instance['title']);
		?>
			<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
		<?php
	}

} // class ConstantContact

// more secure login error reporting
add_filter( 'login_errors', create_function( '$a', "return null;" ) );

// featured-image sizes
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 50, 50, true );
add_image_size( 'featured-items', 530, 78, true );
add_image_size( 'current-series', 796, 448, true );
add_image_size( 'series-art', 700, 394, true );
add_image_size( 'staff', 200, 1000 );
add_image_size( 'gc-news', 700, 1000 );

remove_filter( 'the_excerpt', 'wpautop' );

// Add Navigation Menus
add_theme_support( 'menus' );

register_nav_menus( array(
	'W' => __( 'Weekend Worship Navigation', 'GCTheme' ),
	'MO' => __( 'Missions &amp; Outreach Navigation', 'GCTheme' ),
	'SG' => __( 'Small Groups Navigation', 'GCTheme' ),
	'FM' => __( 'Family Ministry Navigation', 'GCTheme' ),
	'default' => __( 'Default Navigation', 'GCTheme' ),
	'top' => __( 'Top Navigation', 'GCTheme' ),
	'home' => __( 'Home Menu', 'GCTheme' ),
) );

function is_tree( $page ) {
	if ( !is_page() )
		return false;
	if ( is_page( $page ) )
		return true;

	if ( !$anc = get_post_ancestors( get_the_ID() ) )
		return false;

	if ( is_numeric( $page ) )
		$page = get_page( $page );
	elseif ( false !== strpos( $page, ' ' ) )
		$page = get_page_by_title( $page );
	else
		$page = get_page_by_path( $page );

	if ( in_array( $page->ID, $anc ) )
		return true;

	return false;
};



// Custom Post Type Registration
add_action('init', 'cpt_register');

function cpt_register() {
	global $blog_id;

	$labels = array(
		'name'               => _x('Sermon Media', 'post type general name'),
		'singular_name'      => _x('Sermon Media', 'post type singular name'),
		'add_new'            => _x('Add New', 'Sermon Media Post'),
		'add_new_item'       => __('Add New Sermon Media Post'),
		'edit_item'          => __('Edit Sermon Media Post'),
		'new_item'           => __('New Sermon Media Post'),
		'view_item'          => __('View Sermon Media Post'),
		'search_items'       => __('Search Sermon Media Posts'),
		'not_found'          =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon'  => ''
	);

	 $labels2 = array(
		'name'               => _x('Staff Members', 'post type general name'),
		'singular_name'      => _x('Staff Member', 'post type singular name'),
		'add_new'            => _x('Add New', 'Staff Member'),
		'add_new_item'       => __('Add New Staff Member'),
		'edit_item'          => __('Edit Staff Member'),
		'new_item'           => __('New Staff Member'),
		'view_item'          => __('View Staff Member'),
		'search_items'       => __('Search Staff Members'),
		'not_found'          =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon'  => ''
	);

	if ( $blog_id == 1 ) {
		register_post_type( 'sermonaudio' , array(
			'labels'               => $labels,
			'has_archive'          => true,
			'public'               => true,
			'show_ui'              => true,
			'capability_type'      => 'post',
			'menu_position'        => 5,
			'hierarchical'         => false,
			'rewrite'              => array( 'slug' => 'sermon-media' ),
			'register_meta_box_cb' => 'add_cpt_metaboxes',
			'query_var'            => true,
			'supports'             => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'author', 'custom-fields' ),
	   ) );
		register_post_type( 'staff' , array(
			'labels'               => $labels2,
			'public'               => true,
			'show_ui'              => true,
			'capability_type'      => 'post',
			'menu_position'        => 5,
			'hierarchical'         => true,
			'rewrite'              => array( 'slug' => 'staff' ),
			'register_meta_box_cb' => 'add_cpt_metaboxes',
			'query_var'            => true,
			'has_archive'          => true,
			'supports'             => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		) );
		register_post_type( 'ww' , array(
			'label'               => __( 'Large Featured' ),
			'singular_label'      => __( 'Large Featured' ),
			'public'              => true,
			'exclude_from_search' => true,
			'show_ui'             => true,
			'capability_type'     => 'post',
			'menu_position'       => 20,
			'hierarchical'        => false,
			'query_var'           => true,
			'supports'            => array( 'title', 'editor', 'thumbnail' )
	   ) );
		register_post_type( 'featured' , array(
			'label'               => __( 'Small Featured' ),
			'singular_label'      => __( 'Small Featured' ),
			'public'              => true,
			'exclude_from_search' => true,
			'show_ui'             => true,
			'capability_type'     => 'post',
			'_builtin'            => false,
			'menu_position'       => 20,
			'hierarchical'        => false,
			'query_var'           => true,
			'supports'            => array( 'title', 'excerpt', 'thumbnail' )
	   ) );
	}
}



// Custom Taxonomies for Sermon Media
add_action( 'init', 'create_sermonaudio_taxonomies', 0 );
function create_sermonaudio_taxonomies()
{
  // New taxonomy, hierarchical (like categories)
  $labels = array(
	'name' => _x( 'Seasons', 'taxonomy general name' ),
	'singular_name' => _x( 'Season', 'taxonomy singular name' ),
	'search_items' =>  __( 'Search Seasons' ),
	'all_items' => __( 'All Seasons' ),
	'parent_item' => __( 'Parent Season' ),
	'parent_item_colon' => __( 'Parent Season:' ),
	'edit_item' => __( 'Edit Season' ),
	'update_item' => __( 'Update Season' ),
	'add_new_item' => __( 'Add New Season' ),
	'new_item_name' => __( 'New Season Name' ),
  );

  register_taxonomy('season',array('sermonaudio'), array(
	'hierarchical' => true,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'season' ),
  ));

  // New taxonomy, hierarchical (like categories)
  $labels = array(
	'name' => _x( 'Sermon Series', 'taxonomy general name' ),
	'singular_name' => _x( 'Sermon Series', 'taxonomy singular name' ),
	'search_items' =>  __( 'Search Sermon Series' ),
	'all_items' => __( 'All Sermon Series' ),
	'parent_item' => __( 'Parent Sermon Series' ),
	'parent_item_colon' => __( 'Parent Sermon Series:' ),
	'edit_item' => __( 'Edit Sermon Series' ),
	'update_item' => __( 'Update Sermon Series' ),
	'add_new_item' => __( 'Add New Sermon Series' ),
	'new_item_name' => __( 'New Sermon Series Name' ),
  );

  register_taxonomy('series',array('sermonaudio', 'post'), array(
	'hierarchical' => true,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'series' ),
  ));

  // New taxonomy, hierarchical (like categories)
  $labels = array(
	'name' => _x( 'Speakers', 'taxonomy general name' ),
	'singular_name' => _x( 'Speaker', 'taxonomy singular name' ),
	'search_items' =>  __( 'Search Speakers' ),
	'all_items' => __( 'All Speakers' ),
	'parent_item' => __( 'Parent Speaker' ),
	'parent_item_colon' => __( 'Parent Speaker:' ),
	'edit_item' => __( 'Edit Speaker' ),
	'update_item' => __( 'Update Speaker' ),
	'add_new_item' => __( 'Add New Speaker' ),
	'new_item_name' => __( 'New Speaker Name' ),
  );

  register_taxonomy('speaker',array('sermonaudio'), array(
	'hierarchical' => true,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'speaker' ),
  ));

  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
	'name' => _x( 'Topics', 'taxonomy general name' ),
	'singular_name' => _x( 'Topic', 'taxonomy singular name' ),
	'search_items' =>  __( 'Search Topics' ),
	'popular_items' => __( 'Common Topics' ),
	'all_items' => __( 'All Topics' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Topic' ),
	'update_item' => __( 'Update Topic' ),
	'add_new_item' => __( 'Add New Topic' ),
	'new_item_name' => __( 'New Topic Name' ),
	'separate_items_with_commas' => __( 'Separate topics with commas' ),
	'add_or_remove_items' => __( 'Add or remove topics' ),
	'choose_from_most_used' => __( 'Choose from the most used topics' )
  );

  register_taxonomy('topic','sermonaudio',array(
	'hierarchical' => false,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'topic' ),
  ));
}

// Custom Post Types Icons
add_action('admin_head', 'plugin_header');
function plugin_header() {
	global $post_type;
	?>
	<style>
	<?php if (( isset( $_GET['post_type'] ) && $_GET['post_type'] == 'sermonaudio') || ($post_type == 'sermonaudio')) : ?>
	#icon-edit { background:transparent url('<?php echo get_bloginfo('url');?>/wp-admin/images/icons32.png') no-repeat -251px -5px; }
	<?php endif; ?>

	#adminmenu #menu-posts-sermonaudio div.wp-menu-image{background:transparent url('<?php echo get_bloginfo('url');?>/wp-admin/images/menu.png') no-repeat scroll -121px -33px;}
	#adminmenu #menu-posts-sermonaudio:hover div.wp-menu-image,#adminmenu #menu-posts-sermonaudio.wp-has-current-submenu div.wp-menu-image{background:transparent url('<?php echo get_bloginfo('url');?>/wp-admin/images/menu.png') no-repeat scroll -121px -1px;}

	<?php if (( isset( $_GET['post_type'] ) && $_GET['post_type'] == 'staff') || ($post_type == 'staff')) : ?>
	#icon-edit { background:transparent url('<?php echo get_bloginfo('url');?>/wp-admin/images/icons32.png') no-repeat -600px -5px;}
	<?php endif; ?>

	#adminmenu #menu-posts-staff div.wp-menu-image{background:transparent url('<?php echo get_bloginfo('url');?>/wp-admin/images/menu.png') no-repeat scroll -301px -33px;}
	#adminmenu #menu-posts-staff:hover div.wp-menu-image,#adminmenu #menu-posts-staff.wp-has-current-submenu div.wp-menu-image{background:transparent url('<?php echo get_bloginfo('url');?>/wp-admin/images/menu.png') no-repeat scroll -301px -1px;}

	<?php if (( isset( $_GET['post_type'] ) && $_GET['post_type'] == 'ww') || ($post_type == 'ww')) : ?>
	#icon-edit { background:transparent url('<?php echo get_bloginfo('template_directory');?>/images/CPTimages/image--pluslarge.png') no-repeat -14px -5px;}
	<?php endif; ?>

	#adminmenu #menu-posts-ww div.wp-menu-image{background:transparent url('<?php echo get_bloginfo('template_directory');?>/images/CPTimages/image--plus.png') no-repeat 5px -19px;}
	#adminmenu #menu-posts-ww:hover div.wp-menu-image,#adminmenu #menu-posts-ww.wp-has-current-submenu div.wp-menu-image{background:transparent url('<?php echo get_bloginfo('template_directory');?>/images/CPTimages/image--plus.png') no-repeat 5px 5px;}

	<?php if (( isset( $_GET['post_type'] ) && $_GET['post_type'] == 'featured') || ($post_type == 'featured')) : ?>
	#icon-edit {background:transparent url('<?php echo get_bloginfo('template_directory');?>/images/CPTimages/image--minuslarge.png') no-repeat -14px -5px;}
	<?php endif; ?>

	#adminmenu #menu-posts-featured div.wp-menu-image{background:transparent url('<?php echo get_bloginfo('template_directory');?>/images/CPTimages/image--minus.png') no-repeat 5px -19px;}
	#adminmenu #menu-posts-featured:hover div.wp-menu-image,#adminmenu #menu-posts-featured.wp-has-current-submenu div.wp-menu-image{background:transparent url('<?php echo get_bloginfo('template_directory');?>/images/CPTimages/image--minus.png') no-repeat 5px 5px;}
		</style>
		<?php
}



// Custom Taxonomies for Staff

add_action( 'init', 'create_staff_taxonomies', 0 );
function create_staff_taxonomies()
{
  // 1 New taxonomy, hierarchical (like categories) - Staff Positions
  $labels = array(
	'name' => _x( 'Staff Positions', 'taxonomy general name' ),
	'singular_name' => _x( 'Staff Position', 'taxonomy singular name' ),
	'search_items' =>  __( 'Search Staff Positions' ),
	'all_items' => __( 'All Staff Positions' ),
	'parent_item' => __( 'Parent Staff Position' ),
	'parent_item_colon' => __( 'Parent Staff Position:' ),
	'edit_item' => __( 'Edit Staff Position' ),
	'update_item' => __( 'Update Staff Position' ),
	'add_new_item' => __( 'Add New Staff Position' ),
	'new_item_name' => __( 'New Staff Position Name' ),
  );

  register_taxonomy('position',array('staff'), array(
	'hierarchical' => true,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'staff-position' ),
  ));


  // 2 Add new taxonomy, NOT hierarchical (like tags) - Books
  $labels = array(
	'name' => _x( 'Books', 'taxonomy general name' ),
	'singular_name' => _x( 'Book', 'taxonomy singular name' ),
	'search_items' =>  __( 'Search Books' ),
	'popular_items' => __( 'Common Books' ),
	'all_items' => __( 'All Books' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Book' ),
	'update_item' => __( 'Update Book' ),
	'add_new_item' => __( 'Add New Book' ),
	'new_item_name' => __( 'New Book Name' ),
	'separate_items_with_commas' => __( 'Separate books with commas' ),
	'add_or_remove_items' => __( 'Add or remove books' ),
	'choose_from_most_used' => __( 'Choose from the most chosen books' )
  );

  register_taxonomy('books','staff',array(
	'hierarchical' => false,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'staff-favorite-books' ),
  ));

  // 3 Add new taxonomy, NOT hierarchical (like tags) - Movies
  $labels = array(
	'name' => _x( 'Movies', 'taxonomy general name' ),
	'singular_name' => _x( 'Movie', 'taxonomy singular name' ),
	'search_items' =>  __( 'Search Movies' ),
	'popular_items' => __( 'Common Movies' ),
	'all_items' => __( 'All Movies' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Movie' ),
	'update_item' => __( 'Update Movie' ),
	'add_new_item' => __( 'Add New Movie' ),
	'new_item_name' => __( 'New Movie Name' ),
	'separate_items_with_commas' => __( 'Separate movies with commas' ),
	'add_or_remove_items' => __( 'Add or remove movies' ),
	'choose_from_most_used' => __( 'Choose from the most chosen movies' )
  );

  register_taxonomy('movies','staff',array(
	'hierarchical' => false,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'staff-favorite-movies' ),
  ));

  // 4 Add new taxonomy, NOT hierarchical (like tags) - Artists
  $labels = array(
	'name' => _x( 'Artists', 'taxonomy general name' ),
	'singular_name' => _x( 'Artist', 'taxonomy singular name' ),
	'search_items' =>  __( 'Search Artists' ),
	'popular_items' => __( 'Common Artists' ),
	'all_items' => __( 'All Artists' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Artist' ),
	'update_item' => __( 'Update Artist' ),
	'add_new_item' => __( 'Add New Artist' ),
	'new_item_name' => __( 'New Artist Name' ),
	'separate_items_with_commas' => __( 'Separate artists with commas' ),
	'add_or_remove_items' => __( 'Add or remove artists' ),
	'choose_from_most_used' => __( 'Choose from the most chosen artists' )
  );

  register_taxonomy('artists','staff',array(
	'hierarchical' => false,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'staff-favorite-artists' ),
  ));

  // 5 Add new taxonomy, NOT hierarchical (like tags) - Food
  $labels = array(
	'name' => _x( 'Foods', 'taxonomy general name' ),
	'singular_name' => _x( 'Food', 'taxonomy singular name' ),
	'search_items' =>  __( 'Search Foods' ),
	'popular_items' => __( 'Common Foods' ),
	'all_items' => __( 'All Foods' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Food Name' ),
	'update_item' => __( 'Update Food Name' ),
	'add_new_item' => __( 'Add New Food' ),
	'new_item_name' => __( 'New Food Name' ),
	'separate_items_with_commas' => __( 'Separate food names with commas' ),
	'add_or_remove_items' => __( 'Add or remove foods' ),
	'choose_from_most_used' => __( 'Choose from the most chosen foods' )
  );

  register_taxonomy('foods','staff',array(
	'hierarchical' => false,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'staff-favorite-foods' ),
  ));

  // 6 Add new taxonomy, NOT hierarchical (like tags) - Activity
  $labels = array(
	'name' => _x( 'Activities', 'taxonomy general name' ),
	'singular_name' => _x( 'Activity', 'taxonomy singular name' ),
	'search_items' =>  __( 'Search Activities' ),
	'popular_items' => __( 'Common Activities' ),
	'all_items' => __( 'All Activities' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Activity' ),
	'update_item' => __( 'Update Activity' ),
	'add_new_item' => __( 'Add New Activity' ),
	'new_item_name' => __( 'New Activity Name' ),
	'separate_items_with_commas' => __( 'Separate activities with commas' ),
	'add_or_remove_items' => __( 'Add or remove activities' ),
	'choose_from_most_used' => __( 'Choose from the most chosen activities' )
  );

  register_taxonomy('activities','staff',array(
	'hierarchical' => false,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'staff-favorite-activities' ),
  ));

  // 7 Add new taxonomy, NOT hierarchical (like tags) - Verse
  $labels = array(
	'name' => _x( 'Verses', 'taxonomy general name' ),
	'singular_name' => _x( 'Verse', 'taxonomy singular name' ),
	'search_items' =>  __( 'Search Verses' ),
	'popular_items' => __( 'Common Verses' ),
	'all_items' => __( 'All Verses' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Verse' ),
	'update_item' => __( 'Update Verse' ),
	'add_new_item' => __( 'Add New Verse' ),
	'new_item_name' => __( 'New Verse Name' ),
	'separate_items_with_commas' => __( 'Separate verses with commas' ),
	'add_or_remove_items' => __( 'Add or remove verses' ),
	'choose_from_most_used' => __( 'Choose from the most chosen verses' )
  );

  register_taxonomy('verses','staff',array(
	'hierarchical' => false,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'staff-favorite-verses' ),
  ));

  // 8 Add new taxonomy, NOT hierarchical (like tags) - Favorite Things
  $labels = array(
	'name' => _x( 'Other Favorite Things', 'taxonomy general name' ),
	'singular_name' => _x( 'Favorite Thing', 'taxonomy singular name' ),
	'search_items' =>  __( 'Search Favorite Things' ),
	'popular_items' => __( 'Common Favorite Things' ),
	'all_items' => __( 'All Favorite Things' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Favorite Thing' ),
	'update_item' => __( 'Update Favorite Thing' ),
	'add_new_item' => __( 'Add New Favorite Thing' ),
	'new_item_name' => __( 'New Favorite Thing Name' ),
	'separate_items_with_commas' => __( 'Separate favorite things with commas' ),
	'add_or_remove_items' => __( 'Add or remove favorite things' ),
	'choose_from_most_used' => __( 'Choose from the most chosen favorite things' )
  );

  register_taxonomy('favorite-things','staff',array(
	'hierarchical' => false,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'staff-favorite-things' ),
  ));
}



// Custom Taxonomies for Front Page Featured Items

add_action( 'init', 'create_featured_taxonomies', 0 );
function create_featured_taxonomies()
{ register_taxonomy( 'ministry', array('featured', 'ww'), array( 'hierarchical' => true, 'label' => __('Ministry') ) ); }

// Taxonomy conditions
// Topic Taxonomy condition
function has_topic_tag( $topic_tag, $_post = null ) {
	if ( !empty( $topic_tag ) )
		return false;

	if ( $_post )
		$_post = get_post( $_post );
	else
		$_post =& $GLOBALS['post'];

	if ( !$_post )
		return false;

	$r = is_object_in_term( $_post->ID, 'topic', $topic_tag );

	if ( is_wp_error( $r ) )
		return false;

	return $r;
}

// Favorite things Taxonomy condition
function has_favorite_tag( $favorite_tag, $_post = null ) {
	if ( !empty( $favorite_tag ) )
		return false;

	if ( $_post )
		$_post = get_post( $_post );
	else
		$_post =& $GLOBALS['post'];

	if ( !$_post )
		return false;

	$r = is_object_in_term( $_post->ID, 'favorite-things', $topic_tag );

	if ( is_wp_error( $r ) )
		return false;

	return $r;
}

// Activities Taxonomy condition
function has_activity_tag( $activity_tag, $_post = null ) {
	if ( !empty( $activity_tag ) )
		return false;

	if ( $_post )
		$_post = get_post( $_post );
	else
		$_post =& $GLOBALS['post'];

	if ( !$_post )
		return false;

	$r = is_object_in_term( $_post->ID, 'activities', $topic_tag );

	if ( is_wp_error( $r ) )
		return false;

	return $r;
}

// Foods Taxonomy condition
function has_food_tag( $food_tag, $_post = null ) {
	if ( !empty( $food_tag ) )
		return false;

	if ( $_post )
		$_post = get_post( $_post );
	else
		$_post =& $GLOBALS['post'];

	if ( !$_post )
		return false;

	$r = is_object_in_term( $_post->ID, 'foods', $topic_tag );

	if ( is_wp_error( $r ) )
		return false;

	return $r;
}

// Artists Taxonomy condition
function has_artist_tag( $artist_tag, $_post = null ) {
	if ( !empty( $artist_tag ) )
		return false;

	if ( $_post )
		$_post = get_post( $_post );
	else
		$_post =& $GLOBALS['post'];

	if ( !$_post )
		return false;

	$r = is_object_in_term( $_post->ID, 'artists', $topic_tag );

	if ( is_wp_error( $r ) )
		return false;

	return $r;
}


// movies Taxonomy condition
function has_movie_tag( $movie_tag, $_post = null ) {
	if ( !empty( $movie_tag ) )
		return false;

	if ( $_post )
		$_post = get_post( $_post );
	else
		$_post =& $GLOBALS['post'];

	if ( !$_post )
		return false;

	$r = is_object_in_term( $_post->ID, 'movies', $topic_tag );

	if ( is_wp_error( $r ) )
		return false;

	return $r;
}


// verses Taxonomy condition
function has_verse_tag( $verse_tag, $_post = null ) {
	if ( !empty( $verse_tag ) )
		return false;

	if ( $_post )
		$_post = get_post( $_post );
	else
		$_post =& $GLOBALS['post'];

	if ( !$_post )
		return false;

	$r = is_object_in_term( $_post->ID, 'verses', $topic_tag );

	if ( is_wp_error( $r ) )
		return false;

	return $r;
}


// books Taxonomy condition
function has_book_tag( $book_tag, $_post = null ) {
	if ( !empty( $book_tag ) )
		return false;

	if ( $_post )
		$_post = get_post( $_post );
	else
		$_post =& $GLOBALS['post'];

	if ( !$_post )
		return false;

	$r = is_object_in_term( $_post->ID, 'books', $topic_tag );

	if ( is_wp_error( $r ) )
		return false;

	return $r;
}

// positions Taxonomy condition
function has_position_tag( $position_tag, $_post = null ) {
	if ( !empty( $position_tag ) )
		return false;

	if ( $_post )
		$_post = get_post( $_post );
	else
		$_post =& $GLOBALS['post'];

	if ( !$_post )
		return false;

	$r = is_object_in_term( $_post->ID, 'position', $topic_tag );

	if ( is_wp_error( $r ) )
		return false;

	return $r;
}

// Add Plugin Meta Boxes to CPT's
function add_cpt_metaboxes() {
	add_meta_box('Vimeo for Wordpress', __('Vimeo for Wordpress', 'vimeo-for-wordpress'), 'vimeoforwordpress_write_body', 'sermonaudio', 'normal');
	add_meta_box( 'podPressstuff', __('podPress - podcasting settings of this post', 'podpress'), 'podpress_box_content_post', 'sermonaudio', 'advanced' );
	add_meta_box('social_media', 'Social Media', 'social_media', 'staff', 'normal', 'default');
}

// Remove Podpress Functions
remove_filter( 'get_the_excerpt', array( 'podPress_class', 'insert_the_excerpt'), 1 );
remove_filter( 'the_excerpt', array( 'podPress_class', 'insert_the_excerptplayer') );

// Remove Meta Boxes
function remove_stuff() {
	remove_post_type_support( 'post', 'trackbacks' );
	remove_post_type_support( 'page', 'comments' );
	remove_post_type_support( 'page', 'author' );
	remove_post_type_support( 'page', 'excerpt' );
	remove_post_type_support( 'page', 'custom-fields' );
	remove_meta_box( 'podPressstuff', 'post', 'advanced' );
	remove_meta_box( 'podPressstuff', 'page', 'advanced' );
}
add_action( 'admin_menu' , 'remove_stuff' );



// Custom Post Types Appearance


// Custom Post Type: Sermon Media: Appearance
add_action('manage_posts_custom_column', 'sermonaudio_custom_columns');
add_filter("manage_edit-sermonaudio_columns", "sermonaudio_edit_columns");
function sermonaudio_edit_columns($columns){
	$columns = array(
		"cb" => "<input type=\"checkbox\" />",
		"title" => "Sermon Title",
		"date" => "Date Published",
		"description" => "Sermon Description",
		"speaker" => "Speaker",
		"series" => "Sermon Series",
		"season" => "Season",
		"topic" => "Topics",
	);

	return $columns;
}

function sermonaudio_custom_columns($column){
	global $post;
	switch ($column) {
		case "description":
			the_excerpt();
			break;
		case "season":
			echo get_the_term_list($post->ID, 'season', '', ', ','');
			break;
		case "topic":
			echo get_the_term_list($post->ID, 'topic', '', ', ','');
			break;
		case "series":
			echo get_the_term_list($post->ID, 'series', '', ', ','');
			break;
		case "speaker":
			echo get_the_term_list($post->ID, 'speaker', '', ', ','');
			break;
	}
}

// Custom Post Type: Staff: Appearance
add_action('manage_pages_custom_column', 'staff_custom_columns');
add_filter("manage_edit-staff_columns", "staff_edit_columns");

function staff_edit_columns($columns){
	$columns = array(
		"cb" => "<input type=\"checkbox\" />",
		"title" => "Staff Member Name",
		"attributes" => "Order",
		"position" => "Staff Position",
		"verses" => "Favorite Verses",
		"thumbnail" => "Picture",
	);

	return $columns;
}

function staff_custom_columns($column){
	global $post;
	switch ($column) {
		case "attributes":
			echo $post->menu_order ;
			break;
		case "position":
			echo get_the_term_list($post->ID, 'position', '', ', ','');
			break;
		case "verses":
			echo get_the_term_list($post->ID, 'verses', '', ', ','');
			break;
		case "thumbnail":
			the_post_thumbnail( 'thumbnail' );
	}
}

// Register the column as sortable
add_filter( 'manage_edit-staff_sortable_columns', 'staff_custom_sortable' );
function staff_custom_sortable($columns) {
	$columns['attributes'] = 'menu_order';
	return $columns;
}

// staff: Social Media Metabox
function social_media() {
	global $post;
	echo '<input type="hidden" name="social_noncename" id="social_noncename" value="' .
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	$twitter = get_post_meta($post->ID, '_twitter', true);
	$facebook = get_post_meta($post->ID, '_facebook', true);
	$blog = get_post_meta($post->ID, '_blog', true);

		echo '<p>Twitter Username (no @ symbol)</p>';
		echo '<input type="text" name="_twitter" value="' . $twitter  . '" style="width:30%" class="widefat" />';
		echo '<p>Facebook Profile Page (full url including "http://")</p>';
		echo '<input type="text" name="_facebook" value="' . $facebook  . '" style="width:50%" class="widefat" />';
		echo '<p>Blog Address (full url including "http://")</p>';
		echo '<input type="text" name="_blog" value="' . $blog  . '" style="width:50%" class="widefat" />';
}

// Save the Social Media Metabox Data
function wpt_save_social_media_meta($post_id, $post) {
	if ( !wp_verify_nonce( $_POST['social_noncename'], plugin_basename(__FILE__) )) {
	return $post->ID;
	}

	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	$social_meta['_twitter'] = $_POST['_twitter'];
	$social_meta['_facebook'] = $_POST['_facebook'];
	$social_meta['_blog'] = $_POST['_blog'];

	foreach ($social_meta as $key => $value) { // Cycle through the $social_meta array!
		if( $post->post_type == 'revision' ) return; // Don't store custom data twice
		$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
		if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
			update_post_meta($post->ID, $key, $value);
		} else { // If the custom field doesn't have a value
			add_post_meta($post->ID, $key, $value);
		}
		if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
	}

}

add_action('save_post', 'wpt_save_social_media_meta', 1, 2); // save the custom fields


// Custom Post Type: Front Page Large Featured Items: Appearance
add_action('manage_posts_custom_column', 'ww_custom_columns');
add_filter("manage_edit-ww_columns", "ww_edit_columns");

function ww_edit_columns($columns){
	$columns = array(
		"cb" => "<input type=\"checkbox\" />",
		"title" => "Featured Item Name",
		"date" => "Date Published",
		"description" => "Featured Item Description",
		"ministry" => "Ministry Category",
		"thumbnail" => "Thumbnail",
		"link" => "Links To:"
	);

	return $columns;
}
function ww_custom_columns($column){
	global $post;
	switch ($column) {
		case "link":
			$custom = get_post_custom();
			echo $custom["link"][0];
			break;
		case "ministry":
			get_the_term_list($post->ID, 'ministry', '', ', ','');
			break;
		case "description":
			get_the_excerpt();
			break;
		case "thumbnail":
			get_the_post_thumbnail( $post->id, 'thumbnail' );
	}
}

// Large Featured Items: Link meta box
add_action("admin_init", "gc_admin_init");
add_action('save_post', 'save_Link');

function gc_admin_init(){
	add_meta_box("linkbox-meta", "When Clicked Image Navigates To:", "link_box", "ww", "side", "low");
	add_meta_box("textcolor-meta", "Choose Text Color", "gc_text_color_meta", "ww", "side", "low");
}

function link_box(){
	global $post;
	$link = esc_url( get_post_meta($post->ID, 'link', true) );
	?>
		<label>Link: (input full url including "http")</label><br /><br /><input type="text" class="widefat" name="link" value="<?php echo $link; ?>" />
	<?php
}
function gc_text_color_meta(){
	global $post;
	$actualcolor = get_post_meta($post->ID, 'textcolor-meta', true);
	?>
	<p>Text Color: (choose best color for contrast)</p>
	<ul>
		<?php
		$colors = array( 'fff', 'eee', 'ccc', '999', '666', '333', '000' );

		foreach ( $colors as $color ) {


			echo '<li><label for="gc-color-'. $color .'">
					<input type="radio" name="textcolor-meta" id="gc-color-'. $color .'" value="gc-color-'. $color .'"', $actualcolor == 'gc-color-'. $color ? ' checked="checked"' : '', ' />
					<span style="display: block; width: 250px; height: 40px; background: #'. $color .';">Color #'. $color .'</span>';
			echo '</label></li>';
		}
		?>
	</ul>
	<?php
}

function save_link(){
	global $post;
	update_post_meta($post->ID, "link", $_POST["link"]);
	update_post_meta($post->ID, "textcolor-meta", $_POST["textcolor-meta"]);
}


// Custom Post Type: Front Page Small Featured Items: Appearance
add_action('manage_posts_custom_column', 'featured_custom_columns');
add_filter("manage_edit-featured_columns", "featured_edit_columns");

function featured_edit_columns($columns){
	$columns = array(
		"cb" => "<input type=\"checkbox\" />",
		"title" => "Featured Item Name",
		"date" => "Date Published",
		"description" => "Featured Item Excerpt",
		"ministry" => "Ministry Category",
		"thumbnail" => "Thumbnail"
	);

	return $columns;
}

function featured_custom_columns($column){
	global $post;
	switch ($column) {
		case "description":
			get_the_excerpt();
			break;
		case "ministry":
			echo get_the_term_list($post->ID, 'ministry', '', ', ','');
			break;
		case "thumbnail":
			the_post_thumbnail('thumbnail');
			break;
	}
}
