<?php
/*
Template Name: Staff
*/
get_header();

	include('headers/sermon-media.php');
?>

  <div id="content">
  	<div id="leftcolumn">
	<H1 class="staff"><?php the_title(); ?></H1>



<?php
query_posts( array('post_type' => staff,  'paged' => $paged, 'posts_per_page' => 50, 'orderby' => menu_order, 'order' => ASC) );

	 if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>


			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<hr />
				<h3><a href="<?php the_permalink(); ?>"><?php $terms_as_text = get_the_term_list( $post->ID, 'position', '- ', ', ', '' ) ;echo strip_tags($terms_as_text); ?></a></h3>

				<div class="entry">


					<a class="bodylink" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
					<?php the_post_thumbnail( 'staff' ); ?>
					<p><?php the_excerpt() ?></p>
					</a>
				</div>

			</div>
				<hr class="bottomhr" />
		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>

<?php endif;
wp_reset_query();
?>



  	</div>
<div id="rightcolumn">

<div class="staff">
<?php
if(has_position_tag(null)) {

include('sidebars/staff.php'); }

else { ?>
 <h2>Staff Positions</h2><br />
<?php $my_query = new WP_Query( array('post_type' => staff, 'posts_per_page' => 50, 'orderby' => menu_order, 'order' => ASC) ); while ($my_query->have_posts()) : $my_query->the_post(); ?>
<h3 style="margin-top:5px;"><?php $terms_as_text = get_the_term_list( $post->ID, 'position', '', ', ', '' ) ;echo strip_tags($terms_as_text); ?></h3>
<a href="<?php the_permalink(); ?>"><?php the_title('', '');?></a>

<?php
endwhile;
}
?>
</div>

</div>

<?php
    include('footers/sermon-media-footer.php');
?>
