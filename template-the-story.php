<?php
/*
Template Name: "The Story"
*/

function gc_the_story_open() {
	?>
	<script type="text/javascript">
	jQuery(document).ready(function($){
		$link = $( document.getElementById( 'thestory-window') );
		$link.on( 'click', function( evt ) {
			evt.preventDefault();

			var quality = ( screen.width > 1400 ) ? 'high' : 'normal';

			window.open( 'http://viewthestory.com/viewer/?c=14599&p=t&quality='+quality+'', 'Booklet', 'height='+screen.height+',width='+screen.width+',toolbar=no,scrollbars=no,resizable=yes,status=no,copyhistory=no,location=no,menubar=no').focus();
		} );
		setTimeout( function(){$link.trigger( 'click' ); }, 1000 );
	} );
	</script>
	<?php
}
add_action( 'wp_footer', 'gc_the_story_open', 999 );

get_header(); ?>



<?php

if ( is_tree( 'weekend-worship' ) ) {
	include('headers/WW-header.php');

} elseif ( is_tree( 'missions-outreach' ) ) {
	include('headers/M-header.php');

} elseif ( is_tree( 'small-groups' ) ) {
	include('headers/SG-header.php');

} elseif ( is_tree( 'family-ministry' ) ) {
	include('headers/FM-header.php');

} else {
    include('headers/alt-header.php'); // just in case we are at an unclassified page, perhaps the home page
}

?>

  <div id="content">
  	<div id="leftcolumn">

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<H1><?php the_title(); ?></H1>
				<div class="entry">
					<?php the_post_thumbnail( 'gc-news' ); ?>
					<?php the_content(); ?>
					<a id="thestory-window" href="#" border="0" scale="0">
						<img src="http://viewthestory.com/images/embed/TheStory_lg.png" border="0" scale="0">
					</a>
					<?php wp_enqueue_script( 'jquery' ); ?>
					<?php edit_post_link('<br />Edit Page', '', ''); ?>
				</div>


			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>

<?php endif; ?>
</div>

<div id="rightcolumn">

<?php

if ( is_tree('19') ) {
	include('sidebars/WW-sidebar.php');

} elseif ( is_tree('1599') ) {
	include('sidebars/WW-sidebar.php');

// } elseif ( is_tree('17') ) {
// 	include('sidebars/CR-sidebar.php');
} elseif ( is_tree('2703') ) {
	include('sidebars/CR-sidebar.php');

} elseif ( is_tree('21') ) {
	include('sidebars/SG-sidebar.php');

} elseif ( is_tree('20') ) {
	include('sidebars/FM-sidebar.php');

} else {
    include('sidebars/alt-sidebar.php'); // just in case we are at an unclassified page, perhaps the home page
}

?>

</div>

<?php get_footer(); ?>
