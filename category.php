<?php get_header(); ?>

<?php
if (is_category('sermon-Media')) {
include('headers/sermon-media.php');
}

elseif (is_category('12-step-video-teaching') ) {
include("headers/12-step-video-teaching.php");
}

elseif (is_category('troys-blog') ) {
include("headers/troys-blog.php");
}

elseif (is_category('gcblog') ) {
include("headers/troys-blog.php");
}

else {
include("headers/alt-header.php");
}
?>

  <div id="content">
  	<div id="leftcolumn">

<?php if (is_category('sermon-Media')) { ?>
<h1>Sermon Media Archives</h1>
<br />
<p>If you would like to subscribe to automatically receive sermon Media from GC, <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewPodcast?id=327415157">please click here</a>. Make sure you have <a onclick="window.open(this.href); return false;" onkeypress="window.open(this.href); return false;" href="http://www.apple.com/itunes/download" title="http://www.apple.com/itunes/download">iTunes</a> before you subscribe (it&#39;s free). You may also subscribe to the <a href="http://www.generationschurch.com/podcast/feed/">RSS feed</a>.</p>

<?php } else { ?>

<?php } ?>


	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<small><?php the_time('F jS, Y') ?> <!-- by <?php the_author() ?> --></small>

				<div class="entry">
					<?php the_content('Read the rest of this entry &raquo;'); ?>
				</div>

				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &rarr;', '1 Comment &rarr;', '% Comments &rarr;'); ?></p>
			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>

<?php endif; ?>


  	</div>

<?php get_header(); ?>

<?php
if (is_category('sermon-Media')) {
include('sidebars/sermon-media.php');
}

elseif (is_category('12-step-video-teaching') ) {
include("sidebars/12-step-video-teaching.php");
}

elseif (is_category('troys-blog') ) {
include("sidebars/troys-blog.php");
}

elseif (is_category('gcblog') ) {
include("sidebars/sidebar_alternate.php");
}

else {
include("sidebars/sidebar_alternate.php");
}
?>

<?php get_footer(); ?>



