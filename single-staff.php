<?php get_header(); ?>

<?php
	include('headers/WW-header.php');
?>

  <div id="content">
  	<div id="leftcolumn">
	<H1 class="staff"><a href="<?php bloginfo('url'); ?>/meet-the-staff/">Meet The Staff</a></H1>
	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><?php the_title(); ?></h2>
				<h3><?php $terms_as_text = get_the_term_list( $post->ID, 'position', '- ', ', ', '' ) ;echo strip_tags($terms_as_text); ?></h3>


				<div class="entry">
					<?php the_post_thumbnail( 'gc-news' ); ?>
					<?php the_content('Read the rest of this entry &raquo;'); ?>
				</div>
				<p style="clear:both;" class="postmetadata">
						<!--<?php
						$twittername = get_post_meta($post->ID, '_twitter', true);
						if ($twittername == '')
						{} else { ?>
						<a href="http://twitter.com/<?php echo get_post_meta($post->ID, "_twitter", true); ?>">Twitter</a><br />
						<?php } ;
						$facebookurl = get_post_meta($post->ID, '_facebook', true);
						if ($facebookurl == '')
						{} else { ?>
						<a href="<?php echo get_post_meta($post->ID, "_facebook", true); ?>">Facebook</a><br />
						<?php };
						$blogurl = get_post_meta($post->ID, '_blog', true);
						if ($blogurl == '')
						{} else { ?>
						<a href="<?php echo get_post_meta($post->ID, "_blog", true); ?>">Blog</a><br />
						<?php }; ?>--></p>
				<?php edit_post_link('Edit', '', ' | '); ?>
				</div>



		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_post_link('%link', '&larr; Previous Staff Member'); ?></div>
			<div class="alignright"><?php previous_post_link('%link', 'Next Staff Member &rarr;'); ?></div>

		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>

<?php endif; ?>


  	</div>
<div id="rightcolumn">

<div class="staff">
<?php
if(has_position_tag(null)) {

include('sidebars/staff.php'); }

else { ?>
 <h2>Staff Positions</h2>
<?php $my_query = new WP_Query('post_type=staff'); while ($my_query->have_posts()) : $my_query->the_post();
echo get_the_term_list($post->ID, 'position', '', ', ','');

endwhile;
}
?>
</div>

</div>

<?php get_footer(); ?>
