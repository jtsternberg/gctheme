<?php

get_header();

	include('headers/sermon-media.php');
?>

  <div id="content">
  	<div id="leftcolumn">
<h1>Messages</h1>
<p>This is the audio and video archive of the messages from Generations Church.</p>
<p class="staff">If you would like to subscribe to automatically receive podcasts from GC, <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewPodcast?id=327415157">please click here.</a> Make sure you have <a href="http://www.apple.com/itunes/download">iTunes</a> before you subscribe (it's free). You may also subscribe to the <a href="<?php bloginfo('url'); ?>/feed/?post_type=sermon-audio">RSS feed</a>.</p>



<?php
query_posts( array('post_type' => sermonaudio,  'paged' => $paged) );

	 if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2 class="posts"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<hr />
				<small><?php the_time('F jS, Y') ?> by <?php echo get_the_term_list($post->ID,  'speaker', '', ', ', ''); ?> in the series "<?php echo get_the_term_list($post->ID,  'series', '', ', ', ''); ?>"</small>

				<div class="entry">


					<h3 class="gc_excerpt"><em>
					<?php
						if ( !has_excerpt())
						{ echo ''; }
						else { the_excerpt(); }
					?>
					</em></h3>
					<a class="bodylink" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
					<?php the_post_thumbnail( 'series-art' ); ?>
					</a>

					<?php the_content('Read the rest of this entry &raquo;'); ?>

				</div><?php if( function_exists('ADDTOANY_SHARE_SAVE_KIT') ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>

				<p class="postmetadata">
				<?php

echo get_the_term_list($post->ID,  'topic', 'Topics: ', ', ', '');  ?>

				<br /><?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('Click to add comment &rarr;', '1 Comment, click to view &rarr;', '% Comments, click to view &rarr;'); ?></p>

			</div>
				<hr class="bottomhr" />
		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
		</div>

	<?php else : ?>
		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>
		<div class="searchbar2">
		<?php get_template_part( 'repeat_elements/searchform' ); ?>
		</div>

<?php endif;
wp_reset_query();
?>



  	</div>
<div id="rightcolumn">


<?php
	include('sidebars/sermon-media.php');
?>

</div>

<?php
    include('footers/sermon-media-footer.php');
?>
