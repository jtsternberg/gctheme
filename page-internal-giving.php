<!doctype html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title><?php wp_title('&larr;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
  <meta name="robots" content="noindex, nofollow">

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- CSS concatenated and minified via ant build script-->
  <style type="text/css"> 
  	body {
  		background: black url(<?php echo get_stylesheet_directory_uri(); ?>/images/Kiosk_1920x1080.jpg) no-repeat top center;
  		background-attachment: fixed;
  		font-family: 'Century Gothic', Tahoma, Geneva, Verdana, Arial, Helvetica, sans-serif;
  		color: #EEE;
  		font-size: 45px;
  		line-height: 1.2em;
  		padding-bottom: 25px;
  	}
  	section {
  		width: 900px;
  		margin: 100px auto 0;
  		text-align: left;
  	}
  	a {
  		background: transparent url(<?php echo get_stylesheet_directory_uri(); ?>/images/giving.gif) no-repeat top center;
  		display: block;
  		width: 669px;
  		height: 131px;
  		float: right;
  		margin: 10px auto;
  	}
  	em {
  		display: block;
  		font-weight: bold;
  		font-size: 1.4em;
  		line-height: 1.08em;
  		margin-bottom: 20px;
  	}
  	section div p:first-child {
  		width: 840px;
  	}
  </style>
  <!-- end CSS-->
<?php wp_head(); ?>

</head>

<body>

	<section>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<?php the_content(); ?>
			</div>

		<?php endwhile; endif; ?>
	</section>
  
</body>
</html>
